<?php


namespace App\Service;

use App\Entity\AccountUser;
use App\Repository\ProcessFollowingRepository;
use App\Repository\ProcessLikeRepository;
use App\Service\Interfaces\FollowerInterface;
use InstagramAPI\Instagram;
use InstagramAPI\Response\Model\User;
use InstagramAPI\Response\SearchUserResponse;
use Psr\Log\LoggerInterface;

/**
 * Class Follower
 */
class Follower implements FollowerInterface
{
    /**
     * @var InstagramBot
     */
    private $instagram;
    /**
     * @var AccountUser
     */
    private $accountUser;

    /**
     * @var ProcessLikeRepository
     */
    private $processLikeRepository;
    /**
     * @var ProcessFollowingRepository
     */
    private $processFollowRepository;

    /**
     * Follower constructor.
     *
     * @param ProcessFollowingRepository $processFollowRepository
     * @param ProcessLikeRepository      $processLikeRepository
     */
    public function __construct(ProcessFollowingRepository $processFollowRepository, ProcessLikeRepository $processLikeRepository)
    {
        $this->processFollowRepository = $processFollowRepository;
        $this->processLikeRepository = $processLikeRepository;
    }

    /**
     * @param AccountUser $accountUser
     * @param Instagram   $instagram
     *
     * @throws \Exception
     */
    public function follow(AccountUser $accountUser, Instagram $instagram)
    {
        $this->instagram = $instagram;
        $this->accountUser = $accountUser;
        /** @var LoggerInterface $logger */
        $logger = $this->instagram->getLogger();
        $tags = ['baby'];
        $accountUser->getHashTags();
        foreach ($tags as $tag) {
            /** @var SearchUserResponse $stackUsers */
            $stackUsers = $this->instagram->people->search($tag);

            echo $tag . PHP_EOL;
            /** @var User $user */
            foreach ($stackUsers->getUsers() as $user) {
                if ($user->getFriendshipStatus()->getFollowing()) { //  уже подписаны
                    continue;
                }
                $userId = $this->instagram->people->getUserIdForName($user->getUsername());
                $infoUser = $this->instagram->people->getInfoById($user->getPk());
                if (!$infoUser->getUser()->getMediaCount() || $infoUser->getUser()->getIsPrivate()) {
                    if ($this->instagram->debug) {
                        echo 'Current user, continue' . PHP_EOL;
                    }
                    sleep(random_int(random_int(6, 15), random_int(16, 19)));
                    continue;
                }

                $dataMediaUser = $this->instagram->timeline->getUserFeed($user->getPk());
                try {
                    if (!count($dataMediaUser->getItems())) {
                        continue;
                    }
                } catch (\Error $ex) {
                    $logger->warning($ex->getMessage(), ['account' => $accountUser->getLogin(), 'tag' => $tag, 'user' => $user->getUsername()]);
                    sleep(random_int(5, 8));
                    continue;
                } catch (\RuntimeException $ex) {
                    $logger->warning($ex->getMessage(), ['account' => $accountUser->getLogin(), 'tag' => $tag, 'user' => $user->getUsername()]);
                    sleep(random_int(15, 26));
                    continue;
                }
                $like = 0;
                $cntLike = 2;
                $mediaItems = $dataMediaUser->getItems();
                // FOLLOW
                $responseFollow = $this->instagram->people->follow($user->getPk());
                if ($responseFollow instanceof \InstagramAPI\Response\FriendshipResponse && $responseFollow->isOk()) {
                    $logger->debug('Follow user:' . $user->getUsername());
                }
                shuffle($mediaItems);
                foreach ($mediaItems as $item) {
                    if ($userFollowing = $this->hasMediaLike((int)($item->getId() ?? $item->getPk()), $userId ?? $user->getPk())) {
                        break;
                    }
                    // LIKE
                    $responseLike = $this->instagram->media->like($item->getId() ?? $item->getPk());
                    if ($responseLike instanceof \InstagramAPI\Response\GenericResponse && $responseLike->isOk()) {
                        $logger->debug('Liked media: ' . $item->getLink());

                        $like++;
                        if ($like < $cntLike) {
                            sleep(5);
                            continue;
                        }
                        break;
                    }
                }

                echo $userId . PHP_EOL;
                sleep(3);
            }
        }
    }

    /**
     * @param AccountUser $accountUser
     *
     * @return void
     */
    public function unFollow(AccountUser $accountUser, Instagram $instagram)
    {
        echo __METHOD__ . PHP_EOL;
    }

    /**
     * @param string $mediaId
     * @param string $userId
     *
     * @return \App\Entity\ProcessLike|null
     */
    public function hasMediaLike(string $mediaId, string $userId)
    {
        return $this->processLikeRepository->findOneBy([
            'account' => $this->accountUser,
            'likeUserId' => $userId,
            'mediaId' => $mediaId
        ]);
    }
}