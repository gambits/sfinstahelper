<?php


namespace App\Service;

use App\Entity\AccountUser;
use App\Entity\ChallengeRequired;
use App\Service\Interfaces\FollowerInterface;
use App\Service\Interfaces\InstagramBotInterface;
use App\Service\Interfaces\LikerInterface;
use App\Service\Interfaces\PosterInterface;
use Doctrine\ORM\EntityManagerInterface;
use InstagramAPI\Instagram;
use InstagramAPI\Response\LoginResponse;
use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

/**
 * Class InstagramBot
 */
class InstagramBot extends Instagram implements InstagramBotInterface
{
    const TYPE_ACTION_FOLLOW = 'follow';

    const TYPE_ACTION_LIKE = 'like';

    const TYPE_ACTION_UNFOLLOW = 'unfollow';

    const TYPE_ACTION_POST = 'post';

    /**
     * @var LikerInterface
     */
    private $liker;
    /**
     * @var FollowerInterface
     */
    private $follower;
    /**
     * @var PosterInterface
     */
    private $poster;
    /**
     * @var ChallengeRequiredService
     */
    private $challendeRequiredService;
    /**
     * @var string - uniq key for get challengeRequired
     */
    private $hashChallendeRequired;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * InstagramBot constructor.
     *
     * @param bool                     $debug
     * @param bool                     $truncatedDebug
     * @param array                    $storageConfig
     * @param FollowerInterface        $follower
     * @param LikerInterface           $liker
     * @param PosterInterface          $poster
     * @param ChallengeRequiredService $challendeRequired
     *
     * @throws \Exception
     */
    public function __construct(
        $debug = false,
        $truncatedDebug = false,
        array $storageConfig = [],
        FollowerInterface $follower,
        LikerInterface $liker,
        PosterInterface $poster,
        ChallengeRequiredService $challendeRequired
    )
    {
        $this->follower = $follower;
        $this->liker = $liker;
        $this->poster = $poster;
        $this->challendeRequiredService = $challendeRequired;
        parent::__construct($debug, $truncatedDebug, $storageConfig);
    }

    /**
     * @param AccountUser $account
     * @param string      $typeAction
     *
     * @throws \Exception
     */
    public function run(AccountUser $account, string $typeAction): void
    {
        $this->hashChallendeRequired = Uuid::uuid4();
        try {
            /** @var LoginResponse|null $loginResponse */
            $loginResponse = $this->login($account->getLogin(), $account->getPassword());

            if ($loginResponse !== null && $loginResponse->isTwoFactorRequired()) {
                $twoFactorIdentifier = $loginResponse->getTwoFactorInfo()->getTwoFactorIdentifier();
                // The "STDIN" lets you paste the code via terminal for testing.
                // You should replace this line with the logic you want.
                // The verification code will be sent by Instagram via SMS.
                // $verificationCode = trim(fgets(STDIN));
                /** @var ChallengeRequired $challengRequired */
                $challengRequired = $this->getUserResponse($account);
                $this->finishTwoFactorLogin($account->getLogin(), $account->getPassword(), $twoFactorIdentifier, $challengRequired->getCode());
            }
            switch ($typeAction) {
                case self::TYPE_ACTION_FOLLOW:
                    $this->follower->follow($account, $this);
                    break;
                case self::TYPE_ACTION_UNFOLLOW:
                    $this->follower->unFollow($account, $this);
                    break;
                case self::TYPE_ACTION_LIKE:
                    $this->liker->like($account, $this);
                    break;
                case self::TYPE_ACTION_POST:
                    $this->poster->sendPost($account, $this);
                    break;
            }
        } catch (\Exception $exception) {

        }
    }

    /**
     * @param AccountUser $accountUser
     *
     * @return ChallengeRequired
     * @throws \Exception
     */
    public function getUserResponse(AccountUser $accountUser)
    {
        $challengRequired = $this->challendeRequiredService->createChallengeRequired($accountUser, $this->hashChallendeRequired);

        while (true) {
            /** @var ChallengeRequired $challengRequired */
            $isUserRequired = $this->challendeRequiredService->isConfirmLoginResponse($accountUser, $this->hashChallendeRequired);
            if (true === $isUserRequired) {
                break;
            }

            sleep(30);
        }

        return $challengRequired;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return LoggerInterface|null
     */
    public function getLogger()
    {
        return $this->logger;
    }
}