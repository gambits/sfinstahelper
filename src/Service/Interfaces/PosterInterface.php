<?php


namespace App\Service\Interfaces;


use App\Entity\AccountUser;
use InstagramAPI\Instagram;

/**
 * Interface PostInterface
 */
interface PosterInterface
{
    /**
     * @param AccountUser $accountUser
     * @param Instagram   $instagram
     *
     * @return mixed
     */
    public function sendPost(AccountUser $accountUser, Instagram $instagram);
}