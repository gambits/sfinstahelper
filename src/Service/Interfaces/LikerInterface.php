<?php


namespace App\Service\Interfaces;

use App\Entity\AccountUser;
use InstagramAPI\Instagram;

/**
 * Interface LikerInterface
 */
interface LikerInterface
{
    /**
     * @param AccountUser $accountUser
     * @param Instagram   $instagram
     *
     * @return mixed
     */
    public function like(AccountUser $accountUser, Instagram $instagram);
}
