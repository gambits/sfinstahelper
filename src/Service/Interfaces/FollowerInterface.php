<?php


namespace App\Service\Interfaces;

use App\Entity\AccountUser;
use InstagramAPI\Instagram;

/**
 * Interface FollowerInterface
 */
interface FollowerInterface
{
    /**
     * @param AccountUser $accountUser
     * @param Instagram   $instagram
     *
     * @return void
     */
    public function follow(AccountUser $accountUser, Instagram $instagram);

    /**
     * @param AccountUser $accountUser
     * @param Instagram   $instagram
     *
     * @return void
     */
    public function unFollow(AccountUser $accountUser, Instagram $instagram);
}
