<?php


namespace App\Service\Interfaces;

use App\Entity\AccountUser;

/**
 * Interface InstagramBotInterface
 */
interface InstagramBotInterface
{
    public function run(AccountUser $account, string $typeAction): void;
}