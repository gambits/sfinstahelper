<?php


namespace App\Service;


use App\Entity\AccountUser;
use App\Service\Interfaces\PosterInterface;
use InstagramAPI\Instagram;

/**
 * Class Poster
 */
class Poster implements PosterInterface
{
    public function sendPost(AccountUser $accountUser, Instagram $instagram)
    {
        echo __METHOD__ . PHP_EOL;
    }
}