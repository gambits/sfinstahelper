<?php


namespace App\Service;


use App\Entity\AccountUser;
use App\Service\Interfaces\LikerInterface;
use InstagramAPI\Instagram;

/**
 * Class Liker
 */
class Liker implements LikerInterface
{
    /**
     * @param AccountUser $accountUser
     * @param Instagram   $instagram
     *
     * @return void
     */
    public function like(AccountUser $accountUser, Instagram $instagram)
    {
        echo __METHOD__ . PHP_EOL;
    }
}
