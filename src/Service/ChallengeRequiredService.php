<?php


namespace App\Service;

use App\Entity\AccountUser;
use App\Entity\ChallengeRequired;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ChallengeRequiredRepository;

/**
 * Class ChallengeRequiredService
 */
class ChallengeRequiredService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ChallengeRequiredService constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct($em)
    {
        $this->em = $em;
    }

    /**
     * @param AccountUser $accountUser
     * @param string      $hashKey
     *
     * @return ChallengeRequired
     * @throws \Exception
     */
    public function createChallengeRequired(AccountUser $accountUser, $hashKey): ChallengeRequired
    {
        $challengeRequiredEntity = new ChallengeRequired($accountUser);
        $challengeRequiredEntity
            ->setHashKey($hashKey);

        $this->em->persist($challengeRequiredEntity);
        $this->em->flush();
        $this->em->clear($challengeRequiredEntity);

        return $challengeRequiredEntity;
    }

    /**
     * @param AccountUser $accountUser
     *
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function deleteAllChallengeRequired(AccountUser $accountUser)
    {
        return $this->em->createQueryBuilder()
            ->where('account=:account')
            ->setParameter('account', $accountUser)
            ->delete();
    }

    /**
     * @param AccountUser $accountUser
     * @param string      $hashKey
     * @param string|null $code
     *
     * @throws \Exception
     */
    public function setLoginResponse(AccountUser $accountUser, string $hashKey, string $code = null)
    {
        /** @var ChallengeRequired $challengeRequiredEntity */
        $challengeRequiredEntity = $this->em->getRepository(ChallengeRequired::class)->createQueryBuilder('cr')
            ->where('cr.account=:account')
            ->where('cr.hashKey=:key')
            ->setParameter('account', $accountUser)
            ->setParameter('key', $hashKey)
            ->getQuery()
            ->getOneOrNullResult();

        if (!$challengeRequiredEntity instanceof ChallengeRequired) {
            $challengeRequiredEntity = $this->createChallengeRequired($accountUser, $hashKey);
        }

        $challengeRequiredEntity
            ->setCode($code)
            ->setIsResponse(true);

        $this->em->persist($challengeRequiredEntity);
        $this->em->flush();
        $this->em->clear($challengeRequiredEntity);
        gc_collect_cycles();
    }

    /**
     * @param AccountUser $accountUser
     * @param string      $key
     *
     * @return bool
     */
    public function isConfirmLoginResponse(AccountUser $accountUser, $key, bool $isTwoFactor = false)
    {
        $qb = $this->em->getRepository(ChallengeRequired::class)
            ->createQueryBuilder('chr');
        /** @var ChallengeRequired $challengeReq */
        $challengeReq = $qb
            ->where('chr.account=:account')
            ->andWhere('chr.hashKey=:key')
            ->setParameter('account', $accountUser)
            ->setParameter('key', $key)
            ->getQuery()
            ->getResult();

        return $challengeReq->isResponse();
    }
}