<?php

namespace App\Repository;

use App\Entity\IgnoreFollowTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IgnoreFollowTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method IgnoreFollowTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method IgnoreFollowTag[]    findAll()
 * @method IgnoreFollowTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IgnoreFollowTagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IgnoreFollowTag::class);
    }

    // /**
    //  * @return IgnoreFollowTag[] Returns an array of IgnoreFollowTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IgnoreFollowTag
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
