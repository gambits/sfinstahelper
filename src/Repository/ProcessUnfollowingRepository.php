<?php

namespace App\Repository;

use App\Entity\ProcessUnfollowing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProcessUnfollowing|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessUnfollowing|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessUnfollowing[]    findAll()
 * @method ProcessUnfollowing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessUnfollowingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProcessUnfollowing::class);
    }

    // /**
    //  * @return ProcessUnfollowing[] Returns an array of ProcessUnfollowing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessUnfollowing
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
