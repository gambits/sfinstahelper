<?php

namespace App\Repository;

use App\Entity\ShareLikeUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ShareLikeUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShareLikeUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShareLikeUser[]    findAll()
 * @method ShareLikeUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShareLikeUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ShareLikeUser::class);
    }

    // /**
    //  * @return SaleLikeUser[] Returns an array of SaleLikeUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SaleLikeUser
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
