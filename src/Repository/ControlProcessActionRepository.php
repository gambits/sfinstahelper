<?php


namespace App\Repository;

use App\Entity\AccountUser;
use App\Entity\ControlProcessAction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ControlProcessAction|null find($id, $lockMode = null, $lockVersion = null)
 * @method ControlProcessAction|null findOneBy(array $criteria, array $orderBy = null)
 * @method ControlProcessAction[]    findAll()
 * @method ControlProcessAction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ControlProcessActionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ControlProcessAction::class);
    }

    /**
     * @param AccountUser $account
     *
     * @return ControlProcessAction|null
     * @throws \Exception
     */
    public function getOrCreateProcess(AccountUser $account)
    {
        $entity = $this->createQueryBuilder('c')
            ->select('id')
            ->where('`cur_date`=CURRENT_DATE()')
            ->andWhere(['account' => $account])
            ->getQuery()
            ->getResult();

        if (!$entity) {
            $entity = $this->create($account);
        }

        return $entity;
    }

    /**
     * @param AccountUser $account
     *
     * @return ControlProcessAction|null
     * @throws \Exception
     */
    public function create(AccountUser $account): ?ControlProcessAction
    {
        $entity = new ControlProcessAction();
        $entity->setAccount($account)
            ->setCurDate((new \DateTime())->setTimezone(new \DateTimeZone($account->getTimezone())));

        $this->getEntityManager()->persist($entity);
        $this->getEntityManager()->flush();

        return $entity;
    }
}
