<?php

namespace App\Repository;

use App\Entity\ProcessMessageFollow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProcessMessageFollow|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessMessageFollow|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessMessageFollow[]    findAll()
 * @method ProcessMessageFollow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessMessageFollowRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProcessMessageFollow::class);
    }

    // /**
    //  * @return ProcessMessageFollow[] Returns an array of ProcessMessageFollow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessMessageFollow
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
