<?php

namespace App\Repository;

use App\Entity\PostingDataImage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PostingDataImage|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostingDataImage|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostingDataImage[]    findAll()
 * @method PostingDataImage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostingDataImageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostingDataImage::class);
    }

    // /**
    //  * @return PostingDataImage[] Returns an array of PostingDataImage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostingDataImage
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
