<?php

namespace App\Repository;

use App\Entity\AccountLocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountLocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountLocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountLocation[]    findAll()
 * @method AccountLocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountLocationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountLocation::class);
    }

    // /**
    //  * @return AccountLocation[] Returns an array of AccountLocation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountLocation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
