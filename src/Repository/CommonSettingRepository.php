<?php

namespace App\Repository;

use App\Entity\CommonSetting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CommonSetting|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommonSetting|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommonSetting[]    findAll()
 * @method CommonSetting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommonSettingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CommonSetting::class);
    }

    // /**
    //  * @return CommonSetting[] Returns an array of CommonSetting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommonSetting
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
