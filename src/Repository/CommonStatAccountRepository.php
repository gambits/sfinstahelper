<?php

namespace App\Repository;

use App\Entity\CommonStatAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method CommonStatAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method CommonStatAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method CommonStatAccount[]    findAll()
 * @method CommonStatAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommonStatAccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CommonStatAccount::class);
    }

    // /**
    //  * @return CommonStatAccount[] Returns an array of CommonStatAccount objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CommonStatAccount
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
