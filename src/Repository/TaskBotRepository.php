<?php

namespace App\Repository;

use App\Entity\TaskBot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TaskBot|null find($id, $lockMode = null, $lockVersion = null)
 * @method TaskBot|null findOneBy(array $criteria, array $orderBy = null)
 * @method TaskBot[]    findAll()
 * @method TaskBot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskBotRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TaskBot::class);
    }

    // /**
    //  * @return TaskBot[] Returns an array of TaskBot objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TaskBot
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
