<?php

namespace App\Repository;

use App\Entity\ProcessFollowing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProcessFollowing|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessFollowing|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessFollowing[]    findAll()
 * @method ProcessFollowing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessFollowingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProcessFollowing::class);
    }

    // /**
    //  * @return ProcessFollowing[] Returns an array of ProcessFollowing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessFollowing
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
