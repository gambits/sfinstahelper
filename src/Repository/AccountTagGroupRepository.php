<?php

namespace App\Repository;

use App\Entity\AccountTagGroup;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountTagGroup|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountTagGroup|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountTagGroup[]    findAll()
 * @method AccountTagGroup[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountTagGroupRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountTagGroup::class);
    }

    // /**
    //  * @return AccountTagGroup[] Returns an array of AccountTagGroup objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountTagGroup
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
