<?php

namespace App\Repository;

use App\Entity\AccountDirectMessage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountDirectMessage|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountDirectMessage|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountDirectMessage[]    findAll()
 * @method AccountDirectMessage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountDirectMessageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountDirectMessage::class);
    }

    // /**
    //  * @return AccountDirectMessage[] Returns an array of AccountDirectMessage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountDirectMessage
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
