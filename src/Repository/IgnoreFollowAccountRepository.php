<?php


namespace App\Repository;

use App\Entity\IgnoreFollowAccount;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IgnoreFollowAccount|null find($id, $lockMode = null, $lockVersion = null)
 * @method IgnoreFollowAccount|null findOneBy(array $criteria, array $orderBy = null)
 * @method IgnoreFollowAccount[]    findAll()
 * @method IgnoreFollowAccount[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IgnoreFollowAccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IgnoreFollowAccount::class);
    }
}
