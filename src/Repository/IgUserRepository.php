<?php

namespace App\Repository;

use App\Entity\IgUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IgUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method IgUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method IgUser[]    findAll()
 * @method IgUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IgUserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IgUser::class);
    }

    // /**
    //  * @return IgUser[] Returns an array of IgUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IgUser
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
