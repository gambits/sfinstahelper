<?php

namespace App\Repository;

use App\Entity\ProcessPosting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProcessPosting|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessPosting|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessPosting[]    findAll()
 * @method ProcessPosting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessPostingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProcessPosting::class);
    }

    // /**
    //  * @return ProcessPosting[] Returns an array of ProcessPosting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessPosting
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
