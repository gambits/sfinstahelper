<?php

namespace App\Repository;

use App\Entity\IgLoginResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IgLoginResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method IgLoginResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method IgLoginResponse[]    findAll()
 * @method IgLoginResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IgLoginResponseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IgLoginResponse::class);
    }

    // /**
    //  * @return IgLoginResponse[] Returns an array of IgLoginResponse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IgLoginResponse
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
