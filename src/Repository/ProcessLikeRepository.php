<?php

namespace App\Repository;

use App\Entity\ProcessLike;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ProcessLike|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProcessLike|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProcessLike[]    findAll()
 * @method ProcessLike[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProcessLikeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ProcessLike::class);
    }

    // /**
    //  * @return ProcessLike[] Returns an array of ProcessLike objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProcessLike
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
