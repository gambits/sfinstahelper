<?php

namespace App\Repository;

use App\Entity\PostingData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PostingData|null find($id, $lockMode = null, $lockVersion = null)
 * @method PostingData|null findOneBy(array $criteria, array $orderBy = null)
 * @method PostingData[]    findAll()
 * @method PostingData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PostingDataRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PostingData::class);
    }

    // /**
    //  * @return PostingData[] Returns an array of PostingData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PostingData
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
