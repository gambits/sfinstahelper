<?php

namespace App\Repository;

use App\Entity\IgnoreLikeTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method IgnoreLikeTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method IgnoreLikeTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method IgnoreLikeTag[]    findAll()
 * @method IgnoreLikeTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IgnoreLikeTagRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, IgnoreLikeTag::class);
    }

    // /**
    //  * @return IgnoreLikeTag[] Returns an array of IgnoreLikeTag objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?IgnoreLikeTag
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
