<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostingDataImageRepository")
 */
class PostingDataImage
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var PostingData|null
     * @ORM\OneToOne(targetEntity="PostingData")
     */
    private $post;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500)
     */
    private $filepath;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=100)
     */
    private $hash;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $imagePreviewPath;

    /**
     * @var float|null
     * @ORM\Column(type="float")
     */
    private $size = 0;

    /**
     * @var float|null
     * @ORM\Column(type="float")
     */
    private $width = 0;

    /**
     * @var float|null
     * @ORM\Column(type="float")
     */
    private $height = 0;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isVideo = false;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isPreview = false;
    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isAnimation = false;

    /**
     * @return PostingData|null
     */
    public function getPost(): ?PostingData
    {
        return $this->post;
    }

    /**
     * @param PostingData|null $post
     *
     * @return PostingDataImage
     */
    public function setPost(?PostingData $post): PostingDataImage
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilepath(): ?string
    {
        return $this->filepath;
    }

    /**
     * @param string|null $filepath
     *
     * @return PostingDataImage
     */
    public function setFilepath(?string $filepath): PostingDataImage
    {
        $this->filepath = $filepath;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     *
     * @return PostingDataImage
     */
    public function setHash(?string $hash): PostingDataImage
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getImagePreviewPath(): ?string
    {
        return $this->imagePreviewPath;
    }

    /**
     * @param string|null $imagePreviewPath
     *
     * @return PostingDataImage
     */
    public function setImagePreviewPath(?string $imagePreviewPath): PostingDataImage
    {
        $this->imagePreviewPath = $imagePreviewPath;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSize(): ?float
    {
        return $this->size;
    }

    /**
     * @param float|null $size
     *
     * @return PostingDataImage
     */
    public function setSize(?float $size): PostingDataImage
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param float|null $width
     *
     * @return PostingDataImage
     */
    public function setWidth(?float $width): PostingDataImage
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param float|null $height
     *
     * @return PostingDataImage
     */
    public function setHeight(?float $height): PostingDataImage
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsVideo(): ?bool
    {
        return $this->isVideo;
    }

    /**
     * @param bool|null $isVideo
     *
     * @return PostingDataImage
     */
    public function setIsVideo(?bool $isVideo): PostingDataImage
    {
        $this->isVideo = $isVideo;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsPreview(): ?bool
    {
        return $this->isPreview;
    }

    /**
     * @param bool|null $isPreview
     *
     * @return PostingDataImage
     */
    public function setIsPreview(?bool $isPreview): PostingDataImage
    {
        $this->isPreview = $isPreview;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsAnimation(): ?bool
    {
        return $this->isAnimation;
    }

    /**
     * @param bool|null $isAnimation
     *
     * @return PostingDataImage
     */
    public function setIsAnimation(?bool $isAnimation): PostingDataImage
    {
        $this->isAnimation = $isAnimation;

        return $this;
    }
}
