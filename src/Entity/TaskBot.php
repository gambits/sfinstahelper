<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaskBotRepository")
 */
class TaskBot
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @ORM\Column(type="integer")
     */
    private $pid;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isRunning = false;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isStoppedSuccess;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $processType;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $runningDatetime;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $stoppedDatetime;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reasonStop;

    /**
     * @return mixed
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @param mixed $pid
     *
     * @return TaskBot
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRunning(): bool
    {
        return $this->isRunning;
    }

    /**
     * @param bool $isRunning
     *
     * @return TaskBot
     */
    public function setIsRunning(bool $isRunning): TaskBot
    {
        $this->isRunning = $isRunning;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStoppedSuccess(): bool
    {
        return $this->isStoppedSuccess;
    }

    /**
     * @param bool $isStoppedSuccess
     *
     * @return TaskBot
     */
    public function setIsStoppedSuccess(bool $isStoppedSuccess): TaskBot
    {
        $this->isStoppedSuccess = $isStoppedSuccess;

        return $this;
    }

    /**
     * @return string
     */
    public function getProcessType(): string
    {
        return $this->processType;
    }

    /**
     * @param string $processType
     *
     * @return TaskBot
     */
    public function setProcessType(string $processType): TaskBot
    {
        $this->processType = $processType;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRunningDatetime(): \DateTime
    {
        return $this->runningDatetime;
    }

    /**
     * @param \DateTime $runningDatetime
     *
     * @return TaskBot
     */
    public function setRunningDatetime(\DateTime $runningDatetime): TaskBot
    {
        $this->runningDatetime = $runningDatetime;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getStoppedDatetime(): \DateTime
    {
        return $this->stoppedDatetime;
    }

    /**
     * @param \DateTime $stoppedDatetime
     *
     * @return TaskBot
     */
    public function setStoppedDatetime(\DateTime $stoppedDatetime): TaskBot
    {
        $this->stoppedDatetime = $stoppedDatetime;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return TaskBot
     */
    public function setTitle(string $title): TaskBot
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     *
     * @return TaskBot
     */
    public function setComment(string $comment): TaskBot
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string
     */
    public function getReasonStop(): string
    {
        return $this->reasonStop;
    }

    /**
     * @param string $reasonStop
     *
     * @return TaskBot
     */
    public function setReasonStop(string $reasonStop): TaskBot
    {
        $this->reasonStop = $reasonStop;

        return $this;
    }
}
