<?php


namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IgnoreFollowAccountRepository")
 */
class IgnoreFollowAccount
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var AccountUser|null
     * @Assert\NotNull(message="error.validation.not_null.accountLocation.account")
     * @ORM\ManyToOne(targetEntity="AccountUser", inversedBy="ignoreFollowAccount")
     */
    private $account;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $tag;

    /**
     * @return string
     */
    public function getTag(): string
    {
        return $this->tag;
    }

    /**
     * @param string $tag
     *
     * @return IgnoreFollowAccount
     */
    public function setTag(string $tag): IgnoreFollowAccount
    {
        $this->tag = $tag;

        return $this;
    }
}
