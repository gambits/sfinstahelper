<?php


namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountSettingRepository")
 */
class AccountSetting
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;
    /**
     * @var AccountUser|null
     * @Assert\NotNull(message="error.validation.not_null.accountLocation.account")
     * @ORM\OneToOne(targetEntity="AccountUser", inversedBy="settings")
     */
    private $account;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isFollow = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isUnfollow = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isLike = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isComment = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isPosting = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isMessageFollow = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isMessageLike = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", length=1000, nullable=true)
     */
    private $isMessageForDirect = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isLoop = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isAnalize = false;

    /**
     * @return bool
     */
    public function isFollow(): bool
    {
        return $this->isFollow;
    }

    /**
     * @param bool $isFollow
     *
     * @return AccountSetting
     */
    public function setIsFollow(bool $isFollow): AccountSetting
    {
        $this->isFollow = $isFollow;

        return $this;
    }

    /**
     * @return bool
     */
    public function isUnfollow(): bool
    {
        return $this->isUnfollow;
    }

    /**
     * @param bool $isUnfollow
     *
     * @return AccountSetting
     */
    public function setIsUnfollow(bool $isUnfollow): AccountSetting
    {
        $this->isUnfollow = $isUnfollow;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLike(): bool
    {
        return $this->isLike;
    }

    /**
     * @param bool $isLike
     *
     * @return AccountSetting
     */
    public function setIsLike(bool $isLike): AccountSetting
    {
        $this->isLike = $isLike;

        return $this;
    }

    /**
     * @return bool
     */
    public function isComment(): bool
    {
        return $this->isComment;
    }

    /**
     * @param bool $isComment
     *
     * @return AccountSetting
     */
    public function setIsComment(bool $isComment): AccountSetting
    {
        $this->isComment = $isComment;

        return $this;
    }

    /**
     * @return bool
     */
    public function isPosting(): bool
    {
        return $this->isPosting;
    }

    /**
     * @param bool $isPosting
     *
     * @return AccountSetting
     */
    public function setIsPosting(bool $isPosting): AccountSetting
    {
        $this->isPosting = $isPosting;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMessageFollow(): bool
    {
        return $this->isMessageFollow;
    }

    /**
     * @param bool $isMessageFollow
     *
     * @return AccountSetting
     */
    public function setIsMessageFollow(bool $isMessageFollow): AccountSetting
    {
        $this->isMessageFollow = $isMessageFollow;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMessageLike(): bool
    {
        return $this->isMessageLike;
    }

    /**
     * @param bool $isMessageLike
     *
     * @return AccountSetting
     */
    public function setIsMessageLike(bool $isMessageLike): AccountSetting
    {
        $this->isMessageLike = $isMessageLike;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMessageForDirect(): bool
    {
        return $this->isMessageForDirect;
    }

    /**
     * @param bool $isMessageForDirect
     *
     * @return AccountSetting
     */
    public function setIsMessageForDirect(bool $isMessageForDirect): AccountSetting
    {
        $this->isMessageForDirect = $isMessageForDirect;

        return $this;
    }

    /**
     * @return bool
     */
    public function isLoop(): bool
    {
        return $this->isLoop;
    }

    /**
     * @param bool $isLoop
     *
     * @return AccountSetting
     */
    public function setIsLoop(bool $isLoop): AccountSetting
    {
        $this->isLoop = $isLoop;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAnalize(): bool
    {
        return $this->isAnalize;
    }

    /**
     * @param bool $isAnalize
     *
     * @return AccountSetting
     */
    public function setIsAnalize(bool $isAnalize): AccountSetting
    {
        $this->isAnalize = $isAnalize;

        return $this;
    }

    /**
     * @return AccountUser|null
     */
    public function getAccount(): ?AccountUser
    {
        return $this->account;
    }

    /**
     * @param AccountUser|null $account
     *
     * @return AccountSetting
     */
    public function setAccount(?AccountUser $account): AccountSetting
    {
        $this->account = $account;

        return $this;
    }
}
