<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountProxyRepository")
 */
class AccountProxy
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var integer
     * @Assert\NotNull()
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @var integer|null
     * @ORM\Column(type="string", length=255)
     */
    private $port;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isAvailable = false;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isAnonymous = false;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isSsl = false;

    /**
     * @return int
     */
    public function getIp(): int
    {
        return $this->ip;
    }

    /**
     * @param int $ip
     *
     * @return AccountProxy
     */
    public function setIp(int $ip): AccountProxy
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPort(): ?int
    {
        return $this->port;
    }

    /**
     * @param int|null $port
     *
     * @return AccountProxy
     */
    public function setPort(?int $port): AccountProxy
    {
        $this->port = $port;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     *
     * @return AccountProxy
     */
    public function setIsActive(?bool $isActive): AccountProxy
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsAvailable(): ?bool
    {
        return $this->isAvailable;
    }

    /**
     * @param bool|null $isAvailable
     *
     * @return AccountProxy
     */
    public function setIsAvailable(?bool $isAvailable): AccountProxy
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsAnonymous(): ?bool
    {
        return $this->isAnonymous;
    }

    /**
     * @param bool|null $isAnonymous
     *
     * @return AccountProxy
     */
    public function setIsAnonymous(?bool $isAnonymous): AccountProxy
    {
        $this->isAnonymous = $isAnonymous;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsSsl(): ?bool
    {
        return $this->isSsl;
    }

    /**
     * @param bool|null $isSsl
     *
     * @return AccountProxy
     */
    public function setIsSsl(?bool $isSsl): AccountProxy
    {
        $this->isSsl = $isSsl;

        return $this;
    }

}
