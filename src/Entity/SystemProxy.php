<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SystemProxyRepository")
 */
class SystemProxy
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $ip;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $port;

    /**
     * @var string|null
     */
    private $country;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isAvailable = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isSsl = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $isAnonymous = false;

    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }

    /**
     * @param string $ip
     *
     * @return SystemProxy
     */
    public function setIp(string $ip): SystemProxy
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * @return int
     */
    public function getPort(): int
    {
        return $this->port;
    }

    /**
     * @param int $port
     *
     * @return SystemProxy
     */
    public function setPort(int $port): SystemProxy
    {
        $this->port = $port;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     *
     * @return SystemProxy
     */
    public function setIsActive(bool $isActive): SystemProxy
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->isAvailable;
    }

    /**
     * @param bool $isAvailable
     *
     * @return SystemProxy
     */
    public function setIsAvailable(bool $isAvailable): SystemProxy
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSsl(): bool
    {
        return $this->isSsl;
    }

    /**
     * @param bool $isSsl
     *
     * @return SystemProxy
     */
    public function setIsSsl(bool $isSsl): SystemProxy
    {
        $this->isSsl = $isSsl;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAnonymous(): bool
    {
        return $this->isAnonymous;
    }

    /**
     * @param bool $isAnonymous
     *
     * @return SystemProxy
     */
    public function setIsAnonymous(bool $isAnonymous): SystemProxy
    {
        $this->isAnonymous = $isAnonymous;

        return $this;
    }
}
