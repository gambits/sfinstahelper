<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProcessPostingRepository")
 */
class ProcessPosting
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1024)
     */
    private $link;

    /**
     * @var string|null
     * @ORM\Column(type="integer")
     */
    private $posId;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $postedDateTime;

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string|null $link
     *
     * @return ProcessPosting
     */
    public function setLink(?string $link): ProcessPosting
    {
        $this->link = $link;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPosId(): ?string
    {
        return $this->posId;
    }

    /**
     * @param string|null $posId
     *
     * @return ProcessPosting
     */
    public function setPosId(?string $posId): ProcessPosting
    {
        $this->posId = $posId;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPostedDateTime(): \DateTime
    {
        return $this->postedDateTime;
    }

    /**
     * @param \DateTime $postedDateTime
     *
     * @return ProcessPosting
     */
    public function setPostedDateTime(\DateTime $postedDateTime): ProcessPosting
    {
        $this->postedDateTime = $postedDateTime;

        return $this;
    }
}
