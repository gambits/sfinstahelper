<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountDirectMessageRepository")
 */
class AccountDirectMessage
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1000)
     * @Assert\NotNull(message="error.validation.not_null.accountDirectMessage.text")
     */
    private $text;

    /**
     * @var DirectMessageType|null
     * @ORM\OneToOne(targetEntity="DirectMessageType")
     */
    private $type;

    /**
     * AccountDirectMessage constructor.
     *
     * @param AccountUser|null $accountUser
     *
     * @throws \Exception
     */
    public function __construct(?AccountUser $accountUser)
    {
        $this->account = $accountUser;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return DirectMessageType|null
     */
    public function getType(): ?DirectMessageType
    {
        return $this->type;
    }

    /**
     * @param DirectMessageType|null $type
     */
    public function setType(?DirectMessageType $type): void
    {
        $this->type = $type;
    }
}
