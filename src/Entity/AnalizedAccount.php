<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use App\EntityTraits\UserTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnalizedAccountRepository")
 */
class AnalizedAccount
{
    use IdTrait, UserTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $igUserPk;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $countFollow = 0;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $countFollowing = 0;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $countMedia = 0;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $homePictureUrl;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @return string|null
     */
    public function getIgUserPk(): ?string
    {
        return $this->igUserPk;
    }

    /**
     * @param string|null $igUserPk
     *
     * @return AnalizedAccount
     */
    public function setIgUserPk(?string $igUserPk): AnalizedAccount
    {
        $this->igUserPk = $igUserPk;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountFollow(): ?int
    {
        return $this->countFollow;
    }

    /**
     * @param int|null $countFollow
     *
     * @return AnalizedAccount
     */
    public function setCountFollow(?int $countFollow): AnalizedAccount
    {
        $this->countFollow = $countFollow;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountFollowing(): ?int
    {
        return $this->countFollowing;
    }

    /**
     * @param int|null $countFollowing
     *
     * @return AnalizedAccount
     */
    public function setCountFollowing(?int $countFollowing): AnalizedAccount
    {
        $this->countFollowing = $countFollowing;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountMedia(): ?int
    {
        return $this->countMedia;
    }

    /**
     * @param int|null $countMedia
     *
     * @return AnalizedAccount
     */
    public function setCountMedia(?int $countMedia): AnalizedAccount
    {
        $this->countMedia = $countMedia;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return AnalizedAccount
     */
    public function setEmail(?string $email): AnalizedAccount
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return AnalizedAccount
     */
    public function setUsername(?string $username): AnalizedAccount
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHomePictureUrl(): ?string
    {
        return $this->homePictureUrl;
    }

    /**
     * @param string|null $homePictureUrl
     *
     * @return AnalizedAccount
     */
    public function setHomePictureUrl(?string $homePictureUrl): AnalizedAccount
    {
        $this->homePictureUrl = $homePictureUrl;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return AnalizedAccount
     */
    public function setPhone(?string $phone): AnalizedAccount
    {
        $this->phone = $phone;

        return $this;
    }
}
