<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ControlProcessActionRepository")
 */
class ControlProcessAction
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $curDate;

    /**
     * @var int|null
     * @ORM\Column(type="string", length=255)
     */
    private $countLike = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countPosted = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countFollow = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countComment = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countMessageLike = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countMessageFollow = 0;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $lastActionName;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $lastActionTime;

    /**
     * ControlProcessAction constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->curDate = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getCurDate(): \DateTime
    {
        return $this->curDate;
    }

    /**
     * @param \DateTime $curDate
     *
     * @return ControlProcessAction
     */
    public function setCurDate(\DateTime $curDate): ControlProcessAction
    {
        $this->curDate = $curDate;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountLike(): ?int
    {
        return $this->countLike;
    }

    /**
     * @param int|null $countLike
     *
     * @return ControlProcessAction
     */
    public function setCountLike(?int $countLike): ControlProcessAction
    {
        $this->countLike = $countLike;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountPosted(): ?int
    {
        return $this->countPosted;
    }

    /**
     * @param int|null $countPosted
     *
     * @return ControlProcessAction
     */
    public function setCountPosted(?int $countPosted): ControlProcessAction
    {
        $this->countPosted = $countPosted;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountFollow(): ?int
    {
        return $this->countFollow;
    }

    /**
     * @param int|null $countFollow
     *
     * @return ControlProcessAction
     */
    public function setCountFollow(?int $countFollow): ControlProcessAction
    {
        $this->countFollow = $countFollow;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountComment(): ?int
    {
        return $this->countComment;
    }

    /**
     * @param int|null $countComment
     *
     * @return ControlProcessAction
     */
    public function setCountComment(?int $countComment): ControlProcessAction
    {
        $this->countComment = $countComment;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountMessageLike(): ?int
    {
        return $this->countMessageLike;
    }

    /**
     * @param int|null $countMessageLike
     *
     * @return ControlProcessAction
     */
    public function setCountMessageLike(?int $countMessageLike): ControlProcessAction
    {
        $this->countMessageLike = $countMessageLike;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountMessageFollow(): ?int
    {
        return $this->countMessageFollow;
    }

    /**
     * @param int|null $countMessageFollow
     *
     * @return ControlProcessAction
     */
    public function setCountMessageFollow(?int $countMessageFollow): ControlProcessAction
    {
        $this->countMessageFollow = $countMessageFollow;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastActionName(): ?string
    {
        return $this->lastActionName;
    }

    /**
     * @param string|null $lastActionName
     *
     * @return ControlProcessAction
     */
    public function setLastActionName(?string $lastActionName): ControlProcessAction
    {
        $this->lastActionName = $lastActionName;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastActionTime(): \DateTime
    {
        return $this->lastActionTime;
    }

    /**
     * @param \DateTime $lastActionTime
     *
     * @return ControlProcessAction
     */
    public function setLastActionTime(\DateTime $lastActionTime): ControlProcessAction
    {
        $this->lastActionTime = $lastActionTime;

        return $this;
    }
}
