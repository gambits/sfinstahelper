<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostTagRepository")
 */
class PostTag
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;
    /**
     * @var PostingData|null
     * @ORM\OneToOne(targetEntity="PostingData")
     */
    private $post;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $hashTag;

    /**
     * @return PostingData|null
     */
    public function getPost(): ?PostingData
    {
        return $this->post;
    }

    /**
     * @param PostingData|null $post
     *
     * @return PostTag
     */
    public function setPost(?PostingData $post): PostTag
    {
        $this->post = $post;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHashTag(): ?string
    {
        return $this->hashTag;
    }

    /**
     * @param string|null $hashTag
     *
     * @return PostTag
     */
    public function setHashTag(?string $hashTag): PostTag
    {
        $this->hashTag = $hashTag;

        return $this;
    }
}
