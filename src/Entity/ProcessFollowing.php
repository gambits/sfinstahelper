<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProcessFollowingRepository")
 */
class ProcessFollowing
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var int|null
     * @ORM\Column(type="integer")
     */
    private $followingUserId;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1024)
     */
    private $followingUserLink;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $followingTime;

    /**
     * @return int|null
     */
    public function getFollowingUserId(): ?int
    {
        return $this->followingUserId;
    }

    /**
     * @param int|null $followingUserId
     *
     * @return ProcessFollowing
     */
    public function setFollowingUserId(?int $followingUserId): ProcessFollowing
    {
        $this->followingUserId = $followingUserId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFollowingUserLink(): ?string
    {
        return $this->followingUserLink;
    }

    /**
     * @param string|null $followingUserLink
     *
     * @return ProcessFollowing
     */
    public function setFollowingUserLink(?string $followingUserLink): ProcessFollowing
    {
        $this->followingUserLink = $followingUserLink;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getFollowingTime(): ?\DateTime
    {
        return $this->followingTime;
    }

    /**
     * @param \DateTime|null $followingTime
     *
     * @return ProcessFollowing
     */
    public function setFollowingTime(?\DateTime $followingTime): ProcessFollowing
    {
        $this->followingTime = $followingTime;

        return $this;
    }
}
