<?php


namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChallengeRequiredRepository")
 */
class ChallengeRequired
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /** @var string - Confirm code - send to email */
    const TYPE_RESPONSE_CODE_EMAIL = 'CODE_EMAIL';

    /** @var string - Confirm code - send to phone */
    const TYPE_RESPONSE_CODE_PHONE = 'CODE_PHONE';

    /** Confirm from PC or Phone account */
    const TYPE_RESPONSE_OTHER = 'OTHER';

    /**
     * @var AccountUser
     * @ORM\OneToMany(targetEntity="AccountUser", mappedBy="challengeRequired")
     */
    private $account;
    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $code;
    /**
     * @var string
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $type;
    /**
     * @var string
     * @ORM\Column(type="string", length=155, nullable=true)
     */
    private $hashKey;
    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isResponse;

    /**
     * ChallengeRequired constructor.
     *
     * @param AccountUser|null $account
     *
     * @throws \Exception
     */
    public function __construct(?AccountUser $account)
    {
        $this->account = $account;
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return ChallengeRequired
     */
    public function setCode(string $code): ChallengeRequired
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return ChallengeRequired
     */
    public function setType(string $type): ChallengeRequired
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getHashKey(): string
    {
        return $this->hashKey;
    }

    /**
     * @param string $hashKey
     *
     * @return ChallengeRequired
     */
    public function setHashKey(string $hashKey): ChallengeRequired
    {
        $this->hashKey = $hashKey;

        return $this;
    }

    /**
     * @return bool
     */
    public function isResponse(): bool
    {
        return $this->isResponse;
    }

    /**
     * @param bool $isResponse
     *
     * @return ChallengeRequired
     */
    public function setIsResponse(bool $isResponse): ChallengeRequired
    {
        $this->isResponse = $isResponse;

        return $this;
    }
}
