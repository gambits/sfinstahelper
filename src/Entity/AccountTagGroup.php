<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountTagGroupRepository")
 */
class AccountTagGroup
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;
    /**
     * @var AccountUser|null
     * @Assert\NotNull(message="error.validation.not_null.accountLocation.account")
     * @ORM\ManyToOne(targetEntity="AccountUser")
     */
    private $account;
    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $title;
    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $description;

    /**
     * AccountTagGroup constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return AccountTagGroup
     */
    public function setTitle(?string $title): AccountTagGroup
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return AccountTagGroup
     */
    public function setDescription(?string $description): AccountTagGroup
    {
        $this->description = $description;

        return $this;
    }
}
