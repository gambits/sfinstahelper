<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\ProcessActionTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProcessLikeRepository")
 */
class ProcessLike
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait, ProcessActionTrait;
}
