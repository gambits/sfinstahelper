<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DirectMessageTypeRepository")
 */
class DirectMessageType
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return DirectMessageType
     */
    public function setName(string $name): DirectMessageType
    {
        $this->name = $name;

        return $this;
    }
}
