<?php


namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountHashTagRepository")
 */
class AccountHashTag
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;
    /**
     * @var AccountUser|null
     * @Assert\NotNull(message="error.validation.not_null.accountLocation.account")
     * @ORM\ManyToOne(targetEntity="AccountUser", inversedBy="hashTags")
     */
    private $account;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hashTag;

    /**
     * @var AccountTagGroup|null
     * @ORM\OneToOne(targetEntity="AccountTagGroup")
     */
    private $group;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * AccountHashTag constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getHashTag()
    {
        return $this->hashTag;
    }

    /**
     * @param mixed $hashTag
     *
     * @return AccountHashTag
     */
    public function setHashTag($hashTag): AccountHashTag
    {
        $this->hashTag = $hashTag;

        return $this;
    }

    /**
     * @return AccountTagGroup|null
     */
    public function getGroup(): ?AccountTagGroup
    {
        return $this->group;
    }

    /**
     * @param AccountTagGroup|null $group
     *
     * @return AccountHashTag
     */
    public function setGroup(?AccountTagGroup $group): AccountHashTag
    {
        $this->group = $group;

        return $this;
    }
}
