<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommonStatAccountRepository")
 */
class CommonStatAccount
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var integer|null
     * @ORM\Column(type="string", length=255)
     */
    private $userPk;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $countFollow = 0;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $countFollowing = 0;

    /**
     * @var integer|null
     * @ORM\Column(type="float")
     */
    private $countMedia = 0;

    /**
     * @return int|null
     */
    public function getUserPk(): ?int
    {
        return $this->userPk;
    }

    /**
     * @param int|null $userPk
     *
     * @return CommonStatAccount
     */
    public function setUserPk(?int $userPk): CommonStatAccount
    {
        $this->userPk = $userPk;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountFollow(): ?int
    {
        return $this->countFollow;
    }

    /**
     * @param int|null $countFollow
     *
     * @return CommonStatAccount
     */
    public function setCountFollow(?int $countFollow): CommonStatAccount
    {
        $this->countFollow = $countFollow;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountFollowing(): ?int
    {
        return $this->countFollowing;
    }

    /**
     * @param int|null $countFollowing
     *
     * @return CommonStatAccount
     */
    public function setCountFollowing(?int $countFollowing): CommonStatAccount
    {
        $this->countFollowing = $countFollowing;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountMedia(): ?int
    {
        return $this->countMedia;
    }

    /**
     * @param int|null $countMedia
     *
     * @return CommonStatAccount
     */
    public function setCountMedia(?int $countMedia): CommonStatAccount
    {
        $this->countMedia = $countMedia;

        return $this;
    }
}
