<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SystemStatTagsRepository")
 */
class SystemStatTags
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $hashTag;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $mediaCount = 0;

    /**
     * @return string
     */
    public function getHashTag(): string
    {
        return $this->hashTag;
    }

    /**
     * @param string $hashTag
     *
     * @return SystemStatTags
     */
    public function setHashTag(string $hashTag): SystemStatTags
    {
        $this->hashTag = $hashTag;

        return $this;
    }

    /**
     * @return int
     */
    public function getMediaCount(): int
    {
        return $this->mediaCount;
    }

    /**
     * @param int $mediaCount
     *
     * @return SystemStatTags
     */
    public function setMediaCount(int $mediaCount): SystemStatTags
    {
        $this->mediaCount = $mediaCount;

        return $this;
    }
}
