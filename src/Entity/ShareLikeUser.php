<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ShareLikeUserRepository")
 */
class ShareLikeUser
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @var int|null
     * @ORM\Column(type="string", length=255)
     */
    private $countLike = 0;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean")
     */
    private $isRunning = false;

    /**
     * @var bool|null
     * @ORM\Column(type="boolean")
     */
    private $isFinished = false;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1000)
     */
    private $paramsRun;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $taskId;

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string|null $username
     *
     * @return ShareLikeUser
     */
    public function setUsername(?string $username): ShareLikeUser
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountLike(): ?int
    {
        return $this->countLike;
    }

    /**
     * @param int|null $countLike
     *
     * @return ShareLikeUser
     */
    public function setCountLike(?int $countLike): ShareLikeUser
    {
        $this->countLike = $countLike;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsRunning(): ?bool
    {
        return $this->isRunning;
    }

    /**
     * @param bool|null $isRunning
     *
     * @return ShareLikeUser
     */
    public function setIsRunning(?bool $isRunning): ShareLikeUser
    {
        $this->isRunning = $isRunning;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsFinished(): ?bool
    {
        return $this->isFinished;
    }

    /**
     * @param bool|null $isFinished
     *
     * @return ShareLikeUser
     */
    public function setIsFinished(?bool $isFinished): ShareLikeUser
    {
        $this->isFinished = $isFinished;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getParamsRun(): ?string
    {
        return $this->paramsRun;
    }

    /**
     * @param string|null $paramsRun
     *
     * @return ShareLikeUser
     */
    public function setParamsRun(?string $paramsRun): ShareLikeUser
    {
        $this->paramsRun = $paramsRun;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getTaskId(): ?int
    {
        return $this->taskId;
    }

    /**
     * @param int|null $taskId
     *
     * @return ShareLikeUser
     */
    public function setTaskId(?int $taskId): ShareLikeUser
    {
        $this->taskId = $taskId;

        return $this;
    }
}
