<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SystemSettingRepository")
 */
class SystemSetting
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $labelCategory;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $labelKey;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $key;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @return string
     */
    public function getLabelCategory(): string
    {
        return $this->labelCategory;
    }

    /**
     * @param string $labelCategory
     *
     * @return SystemSetting
     */
    public function setLabelCategory(string $labelCategory): SystemSetting
    {
        $this->labelCategory = $labelCategory;

        return $this;
    }

    /**
     * @return string
     */
    public function getCategory(): string
    {
        return $this->category;
    }

    /**
     * @param string $category
     *
     * @return SystemSetting
     */
    public function setCategory(string $category): SystemSetting
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabelKey(): string
    {
        return $this->labelKey;
    }

    /**
     * @param string $labelKey
     *
     * @return SystemSetting
     */
    public function setLabelKey(string $labelKey): SystemSetting
    {
        $this->labelKey = $labelKey;

        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return SystemSetting
     */
    public function setKey(string $key): SystemSetting
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     *
     * @return SystemSetting
     */
    public function setValue(string $value): SystemSetting
    {
        $this->value = $value;

        return $this;
    }
}
