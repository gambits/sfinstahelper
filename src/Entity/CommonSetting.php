<?php

namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommonSettingRepository")
 */
class CommonSetting
{
    use IdTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $alias;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500)
     */
    private $value;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $unitValue;

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return CommonSetting
     */
    public function setTitle(?string $title): CommonSetting
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     *
     * @return CommonSetting
     */
    public function setAlias(?string $alias): CommonSetting
    {
        $this->alias = $alias;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     *
     * @return CommonSetting
     */
    public function setValue(?string $value): CommonSetting
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnitValue(): ?string
    {
        return $this->unitValue;
    }

    /**
     * @param string|null $unitValue
     *
     * @return CommonSetting
     */
    public function setUnitValue(?string $unitValue): CommonSetting
    {
        $this->unitValue = $unitValue;

        return $this;
    }
}
