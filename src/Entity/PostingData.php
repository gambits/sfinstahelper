<?php


namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use App\EntityTraits\UserTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PostingDataRepository")
 */
class PostingData
{
    use IdTrait, UserTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;
    /**
     * @var AccountUser|null
     * @Assert\NotNull(message="error.validation.not_null.accountLocation.account")
     * @ORM\ManyToOne(targetEntity="AccountUser", inversedBy="post")
     */
    private $account;

    /**
     * @var integer|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $igPk;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500)
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $pathFile;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $shortDescription;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $fullDescription;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datePublished;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $linkPost;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isPublished = false;

    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isVideo = false;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $datetimePosting;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=500)
     */
    private $hash;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countLike = 0;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $countComment = 0;
    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $viewCount = 0;

    /**
     * @var int|null
     * @ORM\Column(type="integer", nullable=true)
     */
    private $likeCount = 0;

    /**
     * @return int|null
     */
    public function getIgPk(): ?int
    {
        return $this->igPk;
    }

    /**
     * @param int|null $igPk
     *
     * @return PostingData
     */
    public function setIgPk(?int $igPk): PostingData
    {
        $this->igPk = $igPk;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return PostingData
     */
    public function setTitle(?string $title): PostingData
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPathFile():?string
    {
        return $this->pathFile;
    }

    /**
     * @param string|null $pathFile
     *
     * @return PostingData
     */
    public function setPathFile(?string $pathFile):PostingData
    {
        $this->pathFile = $pathFile;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getShortDescription(): ?string
    {
        return $this->shortDescription;
    }

    /**
     * @param string|null $shortDescription
     *
     * @return PostingData
     */
    public function setShortDescription(?string $shortDescription): PostingData
    {
        $this->shortDescription = $shortDescription;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFullDescription(): ?string
    {
        return $this->fullDescription;
    }

    /**
     * @param string|null $fullDescription
     *
     * @return PostingData
     */
    public function setFullDescription(?string $fullDescription): PostingData
    {
        $this->fullDescription = $fullDescription;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatePublished(): \DateTime
    {
        return $this->datePublished;
    }

    /**
     * @param \DateTime $datePublished
     *
     * @return PostingData
     */
    public function setDatePublished(\DateTime $datePublished): PostingData
    {
        $this->datePublished = $datePublished;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkPost(): ?string
    {
        return $this->linkPost;
    }

    /**
     * @param string|null $linkPost
     *
     * @return PostingData
     */
    public function setLinkPost(?string $linkPost): PostingData
    {
        $this->linkPost = $linkPost;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     *
     * @return PostingData
     */
    public function setIsActive(?bool $isActive): PostingData
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    /**
     * @param bool|null $isPublished
     *
     * @return PostingData
     */
    public function setIsPublished(?bool $isPublished): PostingData
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsVideo(): ?bool
    {
        return $this->isVideo;
    }

    /**
     * @param bool|null $isVideo
     *
     * @return PostingData
     */
    public function setIsVideo(?bool $isVideo): PostingData
    {
        $this->isVideo = $isVideo;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetimePosting(): \DateTime
    {
        return $this->datetimePosting;
    }

    /**
     * @param \DateTime $datetimePosting
     *
     * @return PostingData
     */
    public function setDatetimePosting(\DateTime $datetimePosting): PostingData
    {
        $this->datetimePosting = $datetimePosting;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string|null $hash
     *
     * @return PostingData
     */
    public function setHash(?string $hash): PostingData
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCountLike(): ?int
    {
        return $this->countLike;
    }

    /**
     * @param int|null $countLike
     *
     * @return PostingData
     */
    public function setCountLike(?int $countLike): PostingData
    {
        $this->countLike = $countLike;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountComment()
    {
        return $this->countComment;
    }

    /**
     * @param mixed $countComment
     *
     * @return PostingData
     */
    public function setCountComment($countComment)
    {
        $this->countComment = $countComment;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getViewCount(): ?int
    {
        return $this->viewCount;
    }

    /**
     * @param int|null $viewCount
     *
     * @return PostingData
     */
    public function setViewCount(?int $viewCount): PostingData
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLikeCount(): ?int
    {
        return $this->likeCount;
    }

    /**
     * @param int|null $likeCount
     *
     * @return PostingData
     */
    public function setLikeCount(?int $likeCount): PostingData
    {
        $this->likeCount = $likeCount;

        return $this;
    }
}
