<?php

namespace App\Entity;

use App\EntityTraits\AccountTrait;
use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use App\EntityTraits\UserTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProcessUnfollowingRepository")
 */
class ProcessUnfollowing
{
    use IdTrait, AccountTrait, CreatedAtTrait, UpdatedAtTrait;

    /**
     * @var integer|null
     * @ORM\Column(type="integer")
     */
    private $igUserId;

    /**
     * @return int|null
     */
    public function getIgUserId(): ?int
    {
        return $this->igUserId;
    }

    /**
     * @param int|null $igUserId
     *
     * @return ProcessUnfollowing
     */
    public function setIgUserId(?int $igUserId): ProcessUnfollowing
    {
        $this->igUserId = $igUserId;

        return $this;
    }
}
