<?php


namespace App\Entity;

use App\EntityTraits\CreatedAtTrait;
use App\EntityTraits\IdTrait;
use App\EntityTraits\UpdatedAtTrait;
use App\EntityTraits\UserTrait;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountUserRepository")
 */
class AccountUser
{
    use IdTrait, UserTrait, CreatedAtTrait, UpdatedAtTrait;

    const DEFAULT_TIMEZONE = 'Europe/Moscow';

    /**
     * @var User|null
     * @Assert\NotNull(message="error.validation.not_null.accountDirectMessage.user")
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $login;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $password;
    /**
     * @var ChallengeRequired
     * @ORM\ManyToOne(targetEntity="ChallengeRequired", inversedBy="account")
     */
    private $challengeRequired;
    /**
     * @var array
     * @ORM\Column(type="json_array", options={"jsonb":true})
     */
    private $excludeUnfollowAccount = [];
    /**
     * @var IgnoreFollowTag[]
     * @ORM\OneToMany(targetEntity="IgnoreFollowTag", mappedBy="account")
     */
    private $ignoreFollowTags = [];
    /**
     * @var IgnoreFollowAccount
     * @ORM\OneToMany(targetEntity="IgnoreFollowAccount", mappedBy="account")
     */
    private $ignoreFollowAccount = [];
    /**
     * @var IgnoreLikeTag
     * @ORM\OneToMany(targetEntity="IgnoreLikeTag", mappedBy="account")
     */
    private $ignoreLikeTags = [];
    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    private $isMergeIgnoreTag = false;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $countFollower = 0;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $countFollowing = 0;
    /**
     * @var integer
     * @ORM\Column(type="integer")
     */
    private $countMedia = 0;
    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isActive = false;
    /**
     * @var boolean|null
     * @ORM\Column(type="boolean")
     */
    private $isUseProxy = false;
    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $timezone = self::DEFAULT_TIMEZONE;
    /**
     * @var string|null
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $profilePic;
    /**
     * @var AccountHashTag[]
     * @ORM\OneToMany(targetEntity="AccountHashTag", mappedBy="account")
     */
    private $hashTags = [];
    /**
     * @var PostingData[]
     * @ORM\OneToMany(targetEntity="PostingData", mappedBy="account")
     */
    private $post = [];
    /**
     * @var AccountSetting
     * @ORM\OneToOne(targetEntity="AccountSetting", mappedBy="account")
     */
    private $settings;

    /**
     * AccountUser constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return AccountUser
     */
    public function setTitle(?string $title): AccountUser
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return AccountUser
     */
    public function setDescription(?string $description): AccountUser
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLogin(): ?string
    {
        return $this->login;
    }

    /**
     * @param string|null $login
     *
     * @return AccountUser
     */
    public function setLogin(?string $login): AccountUser
    {
        $this->login = $login;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     *
     * @return AccountUser
     */
    public function setPassword(?string $password): AccountUser
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return array
     */
    public function getExcludeUnfollowAccount(): array
    {
        return $this->excludeUnfollowAccount;
    }

    /**
     * @param array $excludeUnfollowAccount
     */
    public function setExcludeUnfollowAccount(array $excludeUnfollowAccount): void
    {
        $this->excludeUnfollowAccount = $excludeUnfollowAccount;
    }

    /**
     * @return IgnoreFollowTag[]
     */
    public function getIgnoreFollowTags(): array
    {
        return $this->ignoreFollowTags;
    }

    /**
     * @param IgnoreFollowTag[] $ignoreFollowTags
     *
     * @return AccountUser
     */
    public function setIgnoreFollowTags(array $ignoreFollowTags): AccountUser
    {
        $this->ignoreFollowTags = $ignoreFollowTags;

        return $this;
    }

    /**
     * @return IgnoreLikeTag
     */
    public function getIgnoreLikeTags(): IgnoreLikeTag
    {
        return $this->ignoreLikeTags;
    }

    /**
     * @param IgnoreLikeTag $ignoreLikeTags
     *
     * @return AccountUser
     */
    public function setIgnoreLikeTags(IgnoreLikeTag $ignoreLikeTags): AccountUser
    {
        $this->ignoreLikeTags = $ignoreLikeTags;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMergeIgnoreTag(): bool
    {
        return $this->isMergeIgnoreTag;
    }

    /**
     * @param bool $isMergeIgnoreTag
     *
     * @return AccountUser
     */
    public function setIsMergeIgnoreTag(bool $isMergeIgnoreTag): AccountUser
    {
        $this->isMergeIgnoreTag = $isMergeIgnoreTag;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsUseProxy(): ?bool
    {
        return $this->isUseProxy;
    }

    /**
     * @param bool|null $isUseProxy
     *
     * @return AccountUser
     */
    public function setIsUseProxy(?bool $isUseProxy): AccountUser
    {
        $this->isUseProxy = $isUseProxy;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountFollower(): int
    {
        return $this->countFollower;
    }

    /**
     * @param int $countFollower
     *
     * @return AccountUser
     */
    public function setCountFollower(int $countFollower): AccountUser
    {
        $this->countFollower = $countFollower;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountFollowing(): int
    {
        return $this->countFollowing;
    }

    /**
     * @param int $countFollowing
     *
     * @return AccountUser
     */
    public function setCountFollowing(int $countFollowing): AccountUser
    {
        $this->countFollowing = $countFollowing;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountMedia(): int
    {
        return $this->countMedia;
    }

    /**
     * @param int $countMedia
     *
     * @return AccountUser
     */
    public function setCountMedia(int $countMedia): AccountUser
    {
        $this->countMedia = $countMedia;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * @param bool|null $isActive
     *
     * @return AccountUser
     */
    public function setIsActive(?bool $isActive): AccountUser
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTimezone(): ?string
    {
        return $this->timezone;
    }

    /**
     * @param string|null $timezone
     *
     * @return AccountUser
     */
    public function setTimezone(?string $timezone): AccountUser
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProfilePic(): ?string
    {
        return $this->profilePic;
    }

    /**
     * @param string|null $profilePic
     *
     * @return AccountUser
     */
    public function setProfilePic(?string $profilePic): AccountUser
    {
        $this->profilePic = $profilePic;

        return $this;
    }

    /**
     * @return AccountHashTag[]|array
     */
    public function getHashTags(): array
    {
        return $this->hashTags;
    }

    /**
     * @param AccountHashTag[]|null $hashTags
     *
     * @return AccountUser
     */
    public function setHashTags(?array $hashTags): AccountUser
    {
        $this->hashTags = $hashTags;

        return $this;
    }

    /**
     * @return AccountSetting
     */
    public function getSettings(): AccountSetting
    {
        return $this->settings;
    }

    /**
     * @param AccountSetting $settings
     *
     * @return AccountUser
     */
    public function setSettings(AccountSetting $settings): AccountUser
    {
        $this->settings = $settings;

        return $this;
    }
}
