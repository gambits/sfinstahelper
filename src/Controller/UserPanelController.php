<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserPanelController extends AbstractController
{
    /**
     * @Route("/lk", name="user_panel")
     */
    public function index()
    {
        return $this->render('user_panel/index.html.twig', [
            'controller_name' => 'UserPanelController',
        ]);
    }
}
