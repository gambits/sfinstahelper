<?php


namespace App\Controller;


use App\Controller\Base\BaseController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HomeController
 */
class HomeController extends BaseController
{
    /**
     * @Route("/", name="app_home")
     */
    public function home()
    {

        return $this->redirectToRoute('lk_insta_account_list');
    }
}