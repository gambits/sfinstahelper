<?php


namespace App\Controller;


use App\Controller\Base\BaseController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReviewController
 */
class ReviewController extends BaseController
{
    /**
     * @Route("/")
     */
    public function list()
    {

        return $this->render('');
    }
}