<?php


namespace App\Controller\Lk;

use App\Form\PostingDataType;
use App\Repository\PostingDataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class PostingController
 * @Route("/lk/account-posts")
 */
class PostingController extends AbstractController
{
    /**
     * @Route("/", name="lk_account_post_list")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listPost(PostingDataRepository $postingDataRepository)
    {
        return $this->render('lk/posting/post_list.html.twig', [
            'items' => $postingDataRepository->findAll()
        ]);
    }

    /**
     * @Route("/add", name="lk_account_post_add")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addPost()
    {
        $form = $this->createForm(PostingDataType::class);

        return $this->render('lk/posting/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}