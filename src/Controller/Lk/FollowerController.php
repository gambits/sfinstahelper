<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class FollowerController
 * @Route("/lk")
 */
class FollowerController extends AbstractController
{
    /**
     * @Route("/follower", name="lk_follower")
     */
    public function follower()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}