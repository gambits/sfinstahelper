<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LikerController
 * @Route("/lk")
 */
class LikerController extends AbstractController
{
    /**
     * @Route("/liker", name="lk_liker")
     */
    public function liker()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}