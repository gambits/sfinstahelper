<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class TaskBotController
 * @Route("/lk/task-bot")
 */
class TaskBotController extends AbstractController
{
    /**
     * @Route("/", name="lk_task_bot")
     */
    public function index()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}