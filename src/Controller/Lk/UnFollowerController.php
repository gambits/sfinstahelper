<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UnFollowerController
 * @Route("/lk")
 */
class UnFollowerController extends AbstractController
{
    /**
     * @Route("/un-follower", name="lk_unfollower")
     */
    public function unFollower()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}