<?php


namespace App\Controller\Lk;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SystemProxyController
 */
class SystemProxyController extends AbstractController
{
    /**
     * @Route("/list", name="lk_insta_system_account_proxy_list")
     */
    public function list()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }

    /**
     * @Route("/list", name="lk_insta_system_account_proxy_add")
     */
    public function add()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}