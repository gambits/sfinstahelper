<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SettingsController
 * @Route("/lk/setings")
 */
class SettingsController extends AbstractController
{
    /**
     * @Route("/limit-action", name="lk_limits_for_action")
     */
    public function limitsForAction(): Response
    {

        return $this->render('lk/dashboard/index.html.twig');
    }

    /**
     * @Route("/ignore-follower-tag", name="lk_ignore_follower_tag")
     */
    public function ignoreFollowerTag()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }

    /**
     * @Route("/ignore-unfollower-tag", name="lk_ignore_unfollower_tag")
     */
    public function ignoreUnFollowerTag()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }

    /**
     * @Route("/ignore-unfollower-account", name="lk_ignore_unfollower_account")
     */
    public function ignoreUnfollowerAccount()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}