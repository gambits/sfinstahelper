<?php


namespace App\Controller\Lk;

use App\Entity\AccountHashTag;
use App\Entity\AccountTagGroup;
use App\Entity\AccountUser;
use App\Form\AccountHashTagGroupType;
use App\Form\AccountUserType;
use App\Form\DTO\AccountHashTagDto;
use App\Form\AccountHashTagType;
use App\Form\DTO\AccountHashTagGroupDto;
use App\Form\DTO\AccountUserDto;
use App\Repository\AccountHashTagRepository;
use App\Repository\AccountTagGroupRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AccountUser
 * @Route("/lk/insta-account")
 */
class AccountUserController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @Route("/list", name="lk_insta_account_list")
     * @return Response
     */
    public function list(Request $request, EntityManagerInterface $entityManager)
    {
        $dto = new AccountUserDto();
        $form = $this->createForm(AccountUserType::class, $dto);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AccountUserDto $account */
            $account = $form->getData();
            $entity = new AccountUser();
            $entity->setTitle($account->title);
            $entity->setDescription($account->description);
            $entity->setUser($this->getUser());
            $entity->setLogin($account->login);
            $entity->setPassword($account->password);
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('lk_insta_account_list');
        }

        return $this->render('lk/dashboard/index.html.twig', [
            'form' => $form->createView(),
            'accounts' => $entityManager->getRepository(AccountUser::class)->findBy([
                'user' => $this->getUser(),
             ]),
            'titleForm' => 'Accounts',
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/add", name="lk_insta_account_add")
     * @return Response
     */
    public function add(Request $request)
    {
        return $this->render('lk/dashboard/index.html.twig');
    }


    /**
     * @param Request $request
     *
     * @Route("/account-tags", name="lk_insta_account_tag_list")
     * @return Response
     */
    public function tags(AccountHashTagRepository $accountHashTagRepository)
    {
        $dtoAccountHashTag = new AccountHashTagDto();

        $form = $this->createForm(AccountHashTagType::class, $dtoAccountHashTag);

        return $this->render('lk/lk_accounts/tag_list.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @Route("/account-tags-group", name="lk_insta_account_tag_group")
     * @return Response
     */
    public function tagsGroup(AccountTagGroupRepository $accountTagGroupRepository, Request $request, EntityManagerInterface $entityManager)
    {
        $form = $this->createForm(AccountHashTagGroupType::class, new AccountHashTagGroupDto());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /** @var AccountHashTagGroupDto $group */
            $group = $form->getData();
            $entity = new AccountTagGroup();
            $entity->setTitle($group->title);
            $entity->setDescription($group->description);
            $account = $entityManager->getRepository(AccountUser::class)->find($group->account);
            $entity->setAccount($account);
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('lk_insta_account_tag_group');
        }
        return $this->render('lk/lk_accounts/tag_group_list.html.twig', [
            'groups' => $accountTagGroupRepository->findAll(),
            'form' => $form->createView(),
            'titleForm' => 'Create tag group'
        ]);
    }
}