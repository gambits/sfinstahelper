<?php


namespace App\Controller\Lk;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccountUser
 * @Route("/lk/insta-account")
 */
class AnalizeAuditoryController extends AbstractController
{
    /**
     * @param Request $request
     *
     * @Route("/add", name="lk_insta_account_auditory_settings")
     */
    public function add(Request $request)
    {
        return $this->render('lk/dashboard/index.html.twig');
    }
}