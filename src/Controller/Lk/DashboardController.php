<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class DashboardController
 * @Route("/lk")
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/", name="lk_dashboard")
     */
    public function dashboard()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}