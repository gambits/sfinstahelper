<?php


namespace App\Controller\Lk;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SettingsDirectMessageController
 * @Route("/lk/settings")
 */
class SettingsDirectMessageController extends AbstractController
{
    /**
     * @Route("/direct-message", name="lk_direct_message")
     */
    public function directMessage()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }

    /**
     * @Route("/stat-direct-message", name="lk_direct_message_stat")
     */
    public function directMessageStat()
    {

        return $this->render('lk/dashboard/index.html.twig');
    }
}