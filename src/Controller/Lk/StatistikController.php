<?php


namespace App\Controller\Lk;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class StatistikController
 * @Route("/lk/statistik")
 */
class StatistikController extends AbstractController
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="lk_statistik")
     */
    public function dashboard()
    {
        return $this->render('lk/dashboard/index.html.twig');
    }
}