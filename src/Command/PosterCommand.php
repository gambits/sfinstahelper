<?php


namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class PosterCommand
 */
class PosterCommand extends Command
{
    public static $defaultName = 'insta:bot:poster';

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->success('# Running ' . (new \ReflectionClass(__CLASS__))->getShortName());
        $io->newLine();


    }
}