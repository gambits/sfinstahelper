<?php


namespace App\Command;

use App\Entity\AccountUser;
use App\Service\InstagramBot;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class FollowerCommand
 */
class InstagramCommand extends Command
{
    public static $defaultName = 'insta:bot:run';
    /**
     * @var InstagramBot
     */
    private $igBot;

    public function __construct(string $name = null, InstagramBot $igBot)
    {
        $this->igBot = $igBot;
        parent::__construct($name);
    }

    public function configure()
    {
        $this->setDescription('Instagram command start: like, follow, unfollow, analized')
            ->addArgument('accountId', InputArgument::REQUIRED, 'AccountUser ID')
            ->addArgument('typeAction', InputArgument::REQUIRED, 'Type action: like, follow, unfollow, analized');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->success('# Running ' . (new \ReflectionClass(__CLASS__))->getShortName());
        $io->newLine();

        $accountUser = new AccountUser();
        $accountUser->setTimezone('Europe/Moscow');
//        $accountUser->setLogin('babycrazy.ru');
//        $accountUser->setPassword('5eZ1mG8LY17T');

        $accountUser->setLogin('eyebrow_studio_ninel');
        $accountUser->setPassword('Markmaxnina16041604');

        $this->igBot->run($accountUser, $input->getArgument('typeAction'));


        $io->success('#Finished');
    }
}