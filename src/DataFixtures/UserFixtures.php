<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('efin2012@yandex.ru');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'cytu102s62'
        ));
        $manager->persist($user);

        $user = new User();
        $user->setEmail('test@test.ru');
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            'gfhjkm'
        ));
        $manager->persist($user);

        $manager->flush();
    }
}
