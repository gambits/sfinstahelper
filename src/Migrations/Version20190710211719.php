<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190710211719 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE challenge_required DROP is_challenge_required');
        $this->addSql('ALTER TABLE challenge_required DROP is_active');
        $this->addSql('ALTER TABLE process_message_like ALTER like_user_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE process_message_like ALTER like_user_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_message_like ALTER media_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE process_message_like ALTER media_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_message_follow ALTER like_user_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE process_message_follow ALTER like_user_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_message_follow ALTER media_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE process_message_follow ALTER media_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_like ALTER like_user_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE process_like ALTER like_user_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_like ALTER media_id TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE process_like ALTER media_id DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE process_message_like ALTER like_user_id TYPE INT');
        $this->addSql('ALTER TABLE process_message_like ALTER like_user_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_message_like ALTER media_id TYPE INT');
        $this->addSql('ALTER TABLE process_message_like ALTER media_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_message_follow ALTER like_user_id TYPE INT');
        $this->addSql('ALTER TABLE process_message_follow ALTER like_user_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_message_follow ALTER media_id TYPE INT');
        $this->addSql('ALTER TABLE process_message_follow ALTER media_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_like ALTER like_user_id TYPE INT');
        $this->addSql('ALTER TABLE process_like ALTER like_user_id DROP DEFAULT');
        $this->addSql('ALTER TABLE process_like ALTER media_id TYPE INT');
        $this->addSql('ALTER TABLE process_like ALTER media_id DROP DEFAULT');
        $this->addSql('ALTER TABLE challenge_required ADD is_challenge_required BOOLEAN DEFAULT NULL');
        $this->addSql('ALTER TABLE challenge_required ADD is_active BOOLEAN DEFAULT NULL');
    }
}
