<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190708214153 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE account_user DROP CONSTRAINT fk_10051e31be27a4e');
        $this->addSql('DROP SEQUENCE ig_login_response_id_seq CASCADE');
        $this->addSql('DROP TABLE ig_login_response');
        $this->addSql('ALTER TABLE challenge_required ADD type VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE challenge_required ADD hash_key VARCHAR(155) DEFAULT NULL');
        $this->addSql('ALTER TABLE challenge_required ADD is_response BOOLEAN DEFAULT NULL');
        $this->addSql('DROP INDEX idx_10051e31be27a4e');
        $this->addSql('ALTER TABLE account_user DROP ig_login_response_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('CREATE SEQUENCE ig_login_response_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE ig_login_response (id INT NOT NULL, code_sms VARCHAR(50) DEFAULT NULL, code_email VARCHAR(64) DEFAULT NULL, is_active BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE account_user ADD ig_login_response_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE account_user ADD CONSTRAINT fk_10051e31be27a4e FOREIGN KEY (ig_login_response_id) REFERENCES ig_login_response (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_10051e31be27a4e ON account_user (ig_login_response_id)');
        $this->addSql('ALTER TABLE challenge_required DROP type');
        $this->addSql('ALTER TABLE challenge_required DROP hash_key');
        $this->addSql('ALTER TABLE challenge_required DROP is_response');
    }
}
