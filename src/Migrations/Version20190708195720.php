<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190708195720 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE control_process_action_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE process_following_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_hash_tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE common_setting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE posting_data_image_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ig_login_response_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ignore_follow_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE post_tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE "user_id_seq" INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE system_stat_tags_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE analized_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE challenge_required_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE system_setting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE task_bot_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_direct_message_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE posting_data_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_proxy_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_setting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE process_posting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE common_stat_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ignore_like_tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE process_message_like_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE process_unfollowing_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE system_proxy_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE direct_message_type_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ig_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE share_like_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE process_message_follow_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE account_tag_group_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE process_like_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE ignore_follow_tag_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE control_process_action (id INT NOT NULL, account_id INT DEFAULT NULL, cur_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, count_like VARCHAR(255) NOT NULL, count_posted INT DEFAULT NULL, count_follow INT DEFAULT NULL, count_comment INT DEFAULT NULL, count_message_like INT DEFAULT NULL, count_message_follow INT DEFAULT NULL, last_action_name VARCHAR(255) NOT NULL, last_action_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AD4D40F9B6B5FBA ON control_process_action (account_id)');
        $this->addSql('CREATE TABLE process_following (id INT NOT NULL, account_id INT DEFAULT NULL, following_user_id INT NOT NULL, following_user_link VARCHAR(1024) NOT NULL, following_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_EBF6CF0B9B6B5FBA ON process_following (account_id)');
        $this->addSql('CREATE TABLE account_hash_tag (id INT NOT NULL, account_id INT DEFAULT NULL, group_id INT DEFAULT NULL, hash_tag VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7871F3069B6B5FBA ON account_hash_tag (account_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7871F306FE54D947 ON account_hash_tag (group_id)');
        $this->addSql('CREATE TABLE common_setting (id INT NOT NULL, title VARCHAR(255) NOT NULL, alias VARCHAR(255) NOT NULL, value VARCHAR(500) NOT NULL, unit_value VARCHAR(100) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE posting_data_image (id INT NOT NULL, post_id INT DEFAULT NULL, filepath VARCHAR(500) NOT NULL, hash VARCHAR(100) NOT NULL, image_preview_path VARCHAR(255) NOT NULL, size DOUBLE PRECISION NOT NULL, width DOUBLE PRECISION NOT NULL, height DOUBLE PRECISION NOT NULL, is_video BOOLEAN NOT NULL, is_preview BOOLEAN NOT NULL, is_animation BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_68802DAE4B89032C ON posting_data_image (post_id)');
        $this->addSql('CREATE TABLE ig_login_response (id INT NOT NULL, code_sms VARCHAR(50) DEFAULT NULL, code_email VARCHAR(64) DEFAULT NULL, is_active BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ignore_follow_account (id INT NOT NULL, account_id INT DEFAULT NULL, tag VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_63F9CF0F9B6B5FBA ON ignore_follow_account (account_id)');
        $this->addSql('CREATE TABLE post_tag (id INT NOT NULL, post_id INT DEFAULT NULL, hash_tag VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_5ACE3AF04B89032C ON post_tag (post_id)');
        $this->addSql('CREATE TABLE "user" (id INT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('CREATE TABLE system_stat_tags (id INT NOT NULL, hash_tag VARCHAR(255) NOT NULL, media_count INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE analized_account (id INT NOT NULL, user_id INT DEFAULT NULL, account_id INT DEFAULT NULL, ig_user_pk VARCHAR(255) NOT NULL, count_follow INT NOT NULL, count_following INT NOT NULL, count_media INT NOT NULL, email VARCHAR(255) DEFAULT NULL, username VARCHAR(255) NOT NULL, home_picture_url VARCHAR(500) DEFAULT NULL, phone VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A22F2CEDA76ED395 ON analized_account (user_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A22F2CED9B6B5FBA ON analized_account (account_id)');
        $this->addSql('CREATE TABLE challenge_required (id INT NOT NULL, is_challenge_required BOOLEAN DEFAULT NULL, code VARCHAR(64) DEFAULT NULL, is_active BOOLEAN DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE system_setting (id INT NOT NULL, label_category VARCHAR(255) NOT NULL, category VARCHAR(255) NOT NULL, label_key VARCHAR(255) NOT NULL, key VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE task_bot (id INT NOT NULL, account_id INT DEFAULT NULL, pid INT NOT NULL, is_running BOOLEAN NOT NULL, is_stopped_success BOOLEAN NOT NULL, process_type VARCHAR(255) NOT NULL, running_datetime TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, stopped_datetime TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, title VARCHAR(255) NOT NULL, comment VARCHAR(255) DEFAULT NULL, reason_stop VARCHAR(255) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6E9DFC969B6B5FBA ON task_bot (account_id)');
        $this->addSql('CREATE TABLE account_direct_message (id INT NOT NULL, type_id INT DEFAULT NULL, account_id INT DEFAULT NULL, text VARCHAR(1000) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_46E16B99C54C8C93 ON account_direct_message (type_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_46E16B999B6B5FBA ON account_direct_message (account_id)');
        $this->addSql('CREATE TABLE posting_data (id INT NOT NULL, account_id INT DEFAULT NULL, user_id INT DEFAULT NULL, ig_pk INT DEFAULT NULL, title VARCHAR(500) NOT NULL, short_description VARCHAR(1000) DEFAULT NULL, full_description TEXT DEFAULT NULL, date_published TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, link_post VARCHAR(1024) DEFAULT NULL, is_active BOOLEAN NOT NULL, is_published BOOLEAN NOT NULL, is_video BOOLEAN NOT NULL, datetime_posting TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, hash VARCHAR(500) NOT NULL, count_like INT DEFAULT NULL, count_comment INT DEFAULT NULL, view_count INT DEFAULT NULL, like_count INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_1BAA4E889B6B5FBA ON posting_data (account_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1BAA4E88A76ED395 ON posting_data (user_id)');
        $this->addSql('CREATE TABLE account_proxy (id INT NOT NULL, account_id INT DEFAULT NULL, ip VARCHAR(255) NOT NULL, port VARCHAR(255) NOT NULL, is_active BOOLEAN NOT NULL, is_available BOOLEAN NOT NULL, is_anonymous BOOLEAN NOT NULL, is_ssl BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_45FD10CF9B6B5FBA ON account_proxy (account_id)');
        $this->addSql('CREATE TABLE account_setting (id INT NOT NULL, account_id INT DEFAULT NULL, is_follow BOOLEAN NOT NULL, is_unfollow BOOLEAN NOT NULL, is_like BOOLEAN NOT NULL, is_comment BOOLEAN NOT NULL, is_posting BOOLEAN NOT NULL, is_message_follow BOOLEAN NOT NULL, is_message_like BOOLEAN NOT NULL, is_message_for_direct BOOLEAN DEFAULT NULL, is_loop BOOLEAN NOT NULL, is_analize BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_565FACD09B6B5FBA ON account_setting (account_id)');
        $this->addSql('CREATE TABLE process_posting (id INT NOT NULL, account_id INT DEFAULT NULL, link VARCHAR(1024) NOT NULL, pos_id INT NOT NULL, posted_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F55F87A69B6B5FBA ON process_posting (account_id)');
        $this->addSql('CREATE TABLE common_stat_account (id INT NOT NULL, account_id INT DEFAULT NULL, user_pk VARCHAR(255) NOT NULL, count_follow INT NOT NULL, count_following INT NOT NULL, count_media DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CF25DBC19B6B5FBA ON common_stat_account (account_id)');
        $this->addSql('CREATE TABLE ignore_like_tag (id INT NOT NULL, account_id INT DEFAULT NULL, tag VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5D1A31B79B6B5FBA ON ignore_like_tag (account_id)');
        $this->addSql('CREATE TABLE process_message_like (id INT NOT NULL, account_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, like_user_id INT NOT NULL, media_id INT NOT NULL, link_media VARCHAR(1024) NOT NULL, like_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6F488E5B9B6B5FBA ON process_message_like (account_id)');
        $this->addSql('CREATE TABLE account_user (id INT NOT NULL, challenge_required_id INT DEFAULT NULL, ig_login_response_id INT DEFAULT NULL, user_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, login VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, exclude_unfollow_account JSONB NOT NULL, is_merge_ignore_tag BOOLEAN NOT NULL, count_follower INT NOT NULL, count_following INT NOT NULL, count_media INT NOT NULL, is_active BOOLEAN NOT NULL, is_use_proxy BOOLEAN NOT NULL, timezone VARCHAR(255) DEFAULT NULL, profile_pic VARCHAR(500) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_10051E3A4815482 ON account_user (challenge_required_id)');
        $this->addSql('CREATE INDEX IDX_10051E31BE27A4E ON account_user (ig_login_response_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_10051E3A76ED395 ON account_user (user_id)');
        $this->addSql('COMMENT ON COLUMN account_user.exclude_unfollow_account IS \'(DC2Type:json_array)\'');
        $this->addSql('CREATE TABLE process_unfollowing (id INT NOT NULL, account_id INT DEFAULT NULL, ig_user_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_B21F21C29B6B5FBA ON process_unfollowing (account_id)');
        $this->addSql('CREATE TABLE system_proxy (id INT NOT NULL, ip VARCHAR(255) NOT NULL, port INT NOT NULL, is_active BOOLEAN NOT NULL, is_available BOOLEAN NOT NULL, is_ssl BOOLEAN NOT NULL, is_anonymous BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE direct_message_type (id INT NOT NULL, name VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE ig_user (id INT NOT NULL, username VARCHAR(255) NOT NULL, auth_key VARCHAR(255) NOT NULL, password_hash VARCHAR(255) NOT NULL, password_reset_token VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE share_like_user (id INT NOT NULL, account_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, count_like VARCHAR(255) NOT NULL, is_running BOOLEAN NOT NULL, is_finished BOOLEAN NOT NULL, params_run VARCHAR(1000) NOT NULL, task_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_BF11C7BB9B6B5FBA ON share_like_user (account_id)');
        $this->addSql('CREATE TABLE process_message_follow (id INT NOT NULL, account_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, like_user_id INT NOT NULL, media_id INT NOT NULL, link_media VARCHAR(1024) NOT NULL, like_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_824F11089B6B5FBA ON process_message_follow (account_id)');
        $this->addSql('CREATE TABLE account_tag_group (id INT NOT NULL, account_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(500) DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6D5685989B6B5FBA ON account_tag_group (account_id)');
        $this->addSql('CREATE TABLE process_like (id INT NOT NULL, account_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, like_user_id INT NOT NULL, media_id INT NOT NULL, link_media VARCHAR(1024) NOT NULL, like_date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_438A92869B6B5FBA ON process_like (account_id)');
        $this->addSql('CREATE TABLE ignore_follow_tag (id INT NOT NULL, account_id INT DEFAULT NULL, tag VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A29893F29B6B5FBA ON ignore_follow_tag (account_id)');
        $this->addSql('ALTER TABLE control_process_action ADD CONSTRAINT FK_3AD4D40F9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE process_following ADD CONSTRAINT FK_EBF6CF0B9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_hash_tag ADD CONSTRAINT FK_7871F3069B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_hash_tag ADD CONSTRAINT FK_7871F306FE54D947 FOREIGN KEY (group_id) REFERENCES account_tag_group (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE posting_data_image ADD CONSTRAINT FK_68802DAE4B89032C FOREIGN KEY (post_id) REFERENCES posting_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ignore_follow_account ADD CONSTRAINT FK_63F9CF0F9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE post_tag ADD CONSTRAINT FK_5ACE3AF04B89032C FOREIGN KEY (post_id) REFERENCES posting_data (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE analized_account ADD CONSTRAINT FK_A22F2CEDA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE analized_account ADD CONSTRAINT FK_A22F2CED9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE task_bot ADD CONSTRAINT FK_6E9DFC969B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_direct_message ADD CONSTRAINT FK_46E16B99C54C8C93 FOREIGN KEY (type_id) REFERENCES direct_message_type (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_direct_message ADD CONSTRAINT FK_46E16B999B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE posting_data ADD CONSTRAINT FK_1BAA4E889B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE posting_data ADD CONSTRAINT FK_1BAA4E88A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_proxy ADD CONSTRAINT FK_45FD10CF9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_setting ADD CONSTRAINT FK_565FACD09B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE process_posting ADD CONSTRAINT FK_F55F87A69B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE common_stat_account ADD CONSTRAINT FK_CF25DBC19B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ignore_like_tag ADD CONSTRAINT FK_5D1A31B79B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE process_message_like ADD CONSTRAINT FK_6F488E5B9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_user ADD CONSTRAINT FK_10051E3A4815482 FOREIGN KEY (challenge_required_id) REFERENCES challenge_required (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_user ADD CONSTRAINT FK_10051E31BE27A4E FOREIGN KEY (ig_login_response_id) REFERENCES ig_login_response (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_user ADD CONSTRAINT FK_10051E3A76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE process_unfollowing ADD CONSTRAINT FK_B21F21C29B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE share_like_user ADD CONSTRAINT FK_BF11C7BB9B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE process_message_follow ADD CONSTRAINT FK_824F11089B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE account_tag_group ADD CONSTRAINT FK_6D5685989B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE process_like ADD CONSTRAINT FK_438A92869B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE ignore_follow_tag ADD CONSTRAINT FK_A29893F29B6B5FBA FOREIGN KEY (account_id) REFERENCES account_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE account_user DROP CONSTRAINT FK_10051E31BE27A4E');
        $this->addSql('ALTER TABLE analized_account DROP CONSTRAINT FK_A22F2CEDA76ED395');
        $this->addSql('ALTER TABLE posting_data DROP CONSTRAINT FK_1BAA4E88A76ED395');
        $this->addSql('ALTER TABLE account_user DROP CONSTRAINT FK_10051E3A76ED395');
        $this->addSql('ALTER TABLE account_user DROP CONSTRAINT FK_10051E3A4815482');
        $this->addSql('ALTER TABLE posting_data_image DROP CONSTRAINT FK_68802DAE4B89032C');
        $this->addSql('ALTER TABLE post_tag DROP CONSTRAINT FK_5ACE3AF04B89032C');
        $this->addSql('ALTER TABLE control_process_action DROP CONSTRAINT FK_3AD4D40F9B6B5FBA');
        $this->addSql('ALTER TABLE process_following DROP CONSTRAINT FK_EBF6CF0B9B6B5FBA');
        $this->addSql('ALTER TABLE account_hash_tag DROP CONSTRAINT FK_7871F3069B6B5FBA');
        $this->addSql('ALTER TABLE ignore_follow_account DROP CONSTRAINT FK_63F9CF0F9B6B5FBA');
        $this->addSql('ALTER TABLE analized_account DROP CONSTRAINT FK_A22F2CED9B6B5FBA');
        $this->addSql('ALTER TABLE task_bot DROP CONSTRAINT FK_6E9DFC969B6B5FBA');
        $this->addSql('ALTER TABLE account_direct_message DROP CONSTRAINT FK_46E16B999B6B5FBA');
        $this->addSql('ALTER TABLE posting_data DROP CONSTRAINT FK_1BAA4E889B6B5FBA');
        $this->addSql('ALTER TABLE account_proxy DROP CONSTRAINT FK_45FD10CF9B6B5FBA');
        $this->addSql('ALTER TABLE account_setting DROP CONSTRAINT FK_565FACD09B6B5FBA');
        $this->addSql('ALTER TABLE process_posting DROP CONSTRAINT FK_F55F87A69B6B5FBA');
        $this->addSql('ALTER TABLE common_stat_account DROP CONSTRAINT FK_CF25DBC19B6B5FBA');
        $this->addSql('ALTER TABLE ignore_like_tag DROP CONSTRAINT FK_5D1A31B79B6B5FBA');
        $this->addSql('ALTER TABLE process_message_like DROP CONSTRAINT FK_6F488E5B9B6B5FBA');
        $this->addSql('ALTER TABLE process_unfollowing DROP CONSTRAINT FK_B21F21C29B6B5FBA');
        $this->addSql('ALTER TABLE share_like_user DROP CONSTRAINT FK_BF11C7BB9B6B5FBA');
        $this->addSql('ALTER TABLE process_message_follow DROP CONSTRAINT FK_824F11089B6B5FBA');
        $this->addSql('ALTER TABLE account_tag_group DROP CONSTRAINT FK_6D5685989B6B5FBA');
        $this->addSql('ALTER TABLE process_like DROP CONSTRAINT FK_438A92869B6B5FBA');
        $this->addSql('ALTER TABLE ignore_follow_tag DROP CONSTRAINT FK_A29893F29B6B5FBA');
        $this->addSql('ALTER TABLE account_direct_message DROP CONSTRAINT FK_46E16B99C54C8C93');
        $this->addSql('ALTER TABLE account_hash_tag DROP CONSTRAINT FK_7871F306FE54D947');
        $this->addSql('DROP SEQUENCE control_process_action_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE process_following_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_hash_tag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE common_setting_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE posting_data_image_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ig_login_response_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ignore_follow_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE post_tag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE "user_id_seq" CASCADE');
        $this->addSql('DROP SEQUENCE system_stat_tags_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE analized_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE challenge_required_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE system_setting_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE task_bot_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_direct_message_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE posting_data_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_proxy_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_setting_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE process_posting_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE common_stat_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ignore_like_tag_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE process_message_like_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE process_unfollowing_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE system_proxy_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE direct_message_type_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ig_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE share_like_user_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE process_message_follow_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE account_tag_group_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE process_like_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE ignore_follow_tag_id_seq CASCADE');
        $this->addSql('DROP TABLE control_process_action');
        $this->addSql('DROP TABLE process_following');
        $this->addSql('DROP TABLE account_hash_tag');
        $this->addSql('DROP TABLE common_setting');
        $this->addSql('DROP TABLE posting_data_image');
        $this->addSql('DROP TABLE ig_login_response');
        $this->addSql('DROP TABLE ignore_follow_account');
        $this->addSql('DROP TABLE post_tag');
        $this->addSql('DROP TABLE "user"');
        $this->addSql('DROP TABLE system_stat_tags');
        $this->addSql('DROP TABLE analized_account');
        $this->addSql('DROP TABLE challenge_required');
        $this->addSql('DROP TABLE system_setting');
        $this->addSql('DROP TABLE task_bot');
        $this->addSql('DROP TABLE account_direct_message');
        $this->addSql('DROP TABLE posting_data');
        $this->addSql('DROP TABLE account_proxy');
        $this->addSql('DROP TABLE account_setting');
        $this->addSql('DROP TABLE process_posting');
        $this->addSql('DROP TABLE common_stat_account');
        $this->addSql('DROP TABLE ignore_like_tag');
        $this->addSql('DROP TABLE process_message_like');
        $this->addSql('DROP TABLE account_user');
        $this->addSql('DROP TABLE process_unfollowing');
        $this->addSql('DROP TABLE system_proxy');
        $this->addSql('DROP TABLE direct_message_type');
        $this->addSql('DROP TABLE ig_user');
        $this->addSql('DROP TABLE share_like_user');
        $this->addSql('DROP TABLE process_message_follow');
        $this->addSql('DROP TABLE account_tag_group');
        $this->addSql('DROP TABLE process_like');
        $this->addSql('DROP TABLE ignore_follow_tag');
    }
}
