<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190731013230 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP INDEX uniq_3ad4d40f9b6b5fba');
        $this->addSql('CREATE INDEX IDX_3AD4D40F9B6B5FBA ON control_process_action (account_id)');
        $this->addSql('DROP INDEX uniq_ebf6cf0b9b6b5fba');
        $this->addSql('CREATE INDEX IDX_EBF6CF0B9B6B5FBA ON process_following (account_id)');
        $this->addSql('DROP INDEX uniq_a22f2ced9b6b5fba');
        $this->addSql('CREATE INDEX IDX_A22F2CED9B6B5FBA ON analized_account (account_id)');
        $this->addSql('DROP INDEX uniq_6e9dfc969b6b5fba');
        $this->addSql('CREATE INDEX IDX_6E9DFC969B6B5FBA ON task_bot (account_id)');
        $this->addSql('DROP INDEX uniq_46e16b999b6b5fba');
        $this->addSql('CREATE INDEX IDX_46E16B999B6B5FBA ON account_direct_message (account_id)');
        $this->addSql('DROP INDEX uniq_45fd10cf9b6b5fba');
        $this->addSql('CREATE INDEX IDX_45FD10CF9B6B5FBA ON account_proxy (account_id)');
        $this->addSql('DROP INDEX uniq_f55f87a69b6b5fba');
        $this->addSql('CREATE INDEX IDX_F55F87A69B6B5FBA ON process_posting (account_id)');
        $this->addSql('DROP INDEX uniq_cf25dbc19b6b5fba');
        $this->addSql('CREATE INDEX IDX_CF25DBC19B6B5FBA ON common_stat_account (account_id)');
        $this->addSql('DROP INDEX uniq_6f488e5b9b6b5fba');
        $this->addSql('CREATE INDEX IDX_6F488E5B9B6B5FBA ON process_message_like (account_id)');
        $this->addSql('DROP INDEX uniq_b21f21c29b6b5fba');
        $this->addSql('CREATE INDEX IDX_B21F21C29B6B5FBA ON process_unfollowing (account_id)');
        $this->addSql('DROP INDEX uniq_bf11c7bb9b6b5fba');
        $this->addSql('CREATE INDEX IDX_BF11C7BB9B6B5FBA ON share_like_user (account_id)');
        $this->addSql('DROP INDEX uniq_824f11089b6b5fba');
        $this->addSql('CREATE INDEX IDX_824F11089B6B5FBA ON process_message_follow (account_id)');
        $this->addSql('DROP INDEX uniq_438a92869b6b5fba');
        $this->addSql('CREATE INDEX IDX_438A92869B6B5FBA ON process_like (account_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX IDX_3AD4D40F9B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_3ad4d40f9b6b5fba ON control_process_action (account_id)');
        $this->addSql('DROP INDEX IDX_EBF6CF0B9B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_ebf6cf0b9b6b5fba ON process_following (account_id)');
        $this->addSql('DROP INDEX IDX_A22F2CED9B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_a22f2ced9b6b5fba ON analized_account (account_id)');
        $this->addSql('DROP INDEX IDX_6E9DFC969B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_6e9dfc969b6b5fba ON task_bot (account_id)');
        $this->addSql('DROP INDEX IDX_46E16B999B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_46e16b999b6b5fba ON account_direct_message (account_id)');
        $this->addSql('DROP INDEX IDX_45FD10CF9B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_45fd10cf9b6b5fba ON account_proxy (account_id)');
        $this->addSql('DROP INDEX IDX_F55F87A69B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_f55f87a69b6b5fba ON process_posting (account_id)');
        $this->addSql('DROP INDEX IDX_CF25DBC19B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_cf25dbc19b6b5fba ON common_stat_account (account_id)');
        $this->addSql('DROP INDEX IDX_B21F21C29B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_b21f21c29b6b5fba ON process_unfollowing (account_id)');
        $this->addSql('DROP INDEX IDX_BF11C7BB9B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_bf11c7bb9b6b5fba ON share_like_user (account_id)');
        $this->addSql('DROP INDEX IDX_6F488E5B9B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_6f488e5b9b6b5fba ON process_message_like (account_id)');
        $this->addSql('DROP INDEX IDX_824F11089B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_824f11089b6b5fba ON process_message_follow (account_id)');
        $this->addSql('DROP INDEX IDX_438A92869B6B5FBA');
        $this->addSql('CREATE UNIQUE INDEX uniq_438a92869b6b5fba ON process_like (account_id)');
    }
}
