<?php


namespace App\Form;
use App\Form\DTO\AccountUserDto;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class AccountUserType
 */
class AccountUserType extends AbstractType
{
    private $em;

    /**
     * AccountHashTagType constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['attr' => ['placeholder' => 'Label for account Instagram']])
            ->add('login', TextType::class, ['attr' => ['placeholder' => 'Login for account Instagram']])
            ->add('password', TextType::class, ['attr' => ['placeholder' => 'Password for account Instagram']])
            ->add('description', TextType::class, ['attr' => ['placeholder' => 'Description account Instagram']])
            ->add('submit', SubmitType::class)
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AccountUserDto::class
        ]);
    }
}