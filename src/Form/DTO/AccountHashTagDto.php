<?php


namespace App\Form\DTO;

use App\Entity\AccountUser;

/**
 * Class AccountHashTagDto
 */
class AccountHashTagDto
{
    /**
     * @var string|null
     */
    public $hashTag;

    /**
     * @var string|null
     */
    public $newGroup;

    /**
     * @var string|null
     */
    public $group;

    /**
     * @var AccountUser[]
     */
    public $account;
}
