<?php


namespace App\Form\DTO;

/**
 * Class AccountUserDto
 */
class AccountUserDto
{
    /**
     * @var string|null
     */
    public $title;

    /**
     * @var string|null
     */
    public $login;

    /**
     * @var string|null
     */
    public $password;

    /**
     * @var string|null
     */
    public $description;
}