<?php


namespace App\Form\DTO;


use App\Entity\AccountUser;

/**
 * Class AccountHashTagGroupDto
 */
class AccountHashTagGroupDto
{
    /**
     * @var string
     */
    public $title;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var AccountUser
     */
    public $account;
}