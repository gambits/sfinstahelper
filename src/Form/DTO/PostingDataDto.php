<?php


namespace App\Form\DTO;

use App\Entity\PostingDataImage;

/**
 * Class PostingDataDto
 */
class PostingDataDto
{
    /**
     * @var string|null
     */
    public $title;

    /**
     * @var PostingDataImage|null
     */
    public $pathFile;

    /**
     * @var string|null
     */
    public $shortDescription;

    /**
     * @var string|null
     */
    public $fullDescription;

    /**
     * @var boolean|null
     */
    public $isActive = false;

    /**
     * @var boolean|null
     */
    public $isPublished = false;

    /**
     * @var boolean|null
     */
    public $isVideo = false;
}
