<?php


namespace App\Form;
use App\Entity\AccountUser;
use App\Form\DTO\AccountHashTagGroupDto;
use App\Utils\ArrayHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class AccountHashTagGroupType
 */
class AccountHashTagGroupType extends AbstractType
{
    private $em;
    private $tokenStorage;

    /**
     * AccountHashTagType constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accountItems = $this->em->getRepository(AccountUser::class)->findBy(['user'=>$this->tokenStorage->getToken()->getUser()]);

        $builder
            ->add('title', TextType::class)
            ->add('description', TextType::class)
            ->add('account', ChoiceType::class, ['choices' => ArrayHelper::map($accountItems, 'id', 'login')])
            ->add('submit', SubmitType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AccountHashTagGroupDto::class
        ]);
    }
}