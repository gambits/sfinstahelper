<?php


namespace App\Form;

use App\Entity\AccountTagGroup;
use App\Entity\AccountUser;
use App\Form\DTO\AccountHashTagDto;
use App\Utils\ArrayHelper;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class AccountHashTagType extends AbstractType
{
    private $em;
    private $tokenStorage;

    /**
     * AccountHashTagType constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $accountItems = $this->em->getRepository(AccountUser::class)->findBy(['user'=>$this->tokenStorage->getToken()->getUser()]);
        $accountGroups = $this->em->getRepository(AccountTagGroup::class)->findBy(['user'=>$this->tokenStorage->getToken()->getUser()]);

        $builder
            ->add('hashTag', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('group', ChoiceType::class, ['choices' => ArrayHelper::map($accountGroups, 'id', 'login'), 'attr' => ['class' => 'form-control']])
            ->add('newGroup', TextType::class, ['attr' => ['class' => 'form-control']])
            ->add('account', ChoiceType::class, ['choices' => ArrayHelper::map($accountItems, 'id', 'login')], ['attr' => ['class' => 'form-control']])
            ->add('submit', SubmitType::class, ['attr' => ['class' => 'btn btn-block btn-primary']])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AccountHashTagDto::class
        ]);
    }
}
