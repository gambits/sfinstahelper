<?php


namespace App\Form;

use App\Entity\PostingDataImage;
use App\Form\DTO\PostingDataDto;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class PostingDataDto
 */
class PostingDataType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('pathFile', EntityType::class, [
                'class' => PostingDataImage::class
            ])
            ->add('shortDescription', TextType::class, [])
            ->add('fullDescription', TextType::class, [])
            ->add('isActive', CheckboxType::class, [])
            ->add('isPublished', CheckboxType::class, [])
            ->add('isVideo', CheckboxType::class, [])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PostingDataDto::class,
        ]);
    }
}
