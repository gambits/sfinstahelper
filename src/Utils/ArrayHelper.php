<?php


namespace App\Utils;

/**
 * Class ArrayHelper
 */
class ArrayHelper
{
    /**
     * @param array  $data
     * @param string $from
     * @param string $to
     *
     * @return array
     */
    public static function map(array $data, string $from, string $to): array
    {
        $items = [];

        foreach ($data as $item) {
            if (is_object($item)) {
                $methodGetFrom = 'get'.ucfirst($from);
                $methodGetTo = 'get'.ucfirst($to);
                $items[$item->$methodGetTo()] = $item->$methodGetFrom();
            } else {
                $items[$item[$from]] = $item[$to];
            }
        }

        return $items;
    }
}