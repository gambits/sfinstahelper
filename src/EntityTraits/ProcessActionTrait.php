<?php


namespace App\EntityTraits;


/**
 * Trait ProcessActionTrait
 */
trait ProcessActionTrait
{
    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $likeUserId;

    /**
     * @var string|null
     * @ORM\Column(type="string")
     */
    private $mediaId;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=1024)
     */
    private $linkMedia;

    /**
     * @var \DateTime|null
     * @ORM\Column(type="datetime")
     */
    private $likeDateTime;

    /**
     * @return string|null
     */
    public function getLikeUserId(): ?string
    {
        return $this->likeUserId;
    }

    /**
     * @param string|null $likeUserId
     *
     * @return $this
     */
    public function setLikeUserId(?string $likeUserId): self
    {
        $this->likeUserId = $likeUserId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMediaId(): ?string
    {
        return $this->mediaId;
    }

    /**
     * @param string|null $mediaId
     *
     * @return $this
     */
    public function setMediaId(?string $mediaId): self
    {
        $this->mediaId = $mediaId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLinkMedia(): ?string
    {
        return $this->linkMedia;
    }

    /**
     * @param string|null $linkMedia
     *
     * @return $this
     */
    public function setLinkMedia(?string $linkMedia): self
    {
        $this->linkMedia = $linkMedia;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLikeDateTime(): ?\DateTime
    {
        return $this->likeDateTime;
    }

    /**
     * @param \DateTime|null $likeDateTime
     *
     * @return $this
     */
    public function setLikeDateTime(?\DateTime $likeDateTime): self
    {
        $this->likeDateTime = $likeDateTime;

        return $this;
    }
}