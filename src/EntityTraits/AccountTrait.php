<?php


namespace App\EntityTraits;


use App\Entity\AccountUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait AccountTrait
 */
trait AccountTrait
{
    /**
     * @var AccountUser|null
     * @Assert\NotNull(message="error.validation.not_null.accountLocation.account")
     * @ORM\ManyToOne(targetEntity="AccountUser")
     */
    private $account;

    /**
     * @return AccountUser|null
     */
    public function getAccount(): ?AccountUser
    {
        return $this->account;
    }

    /**
     * @param AccountUser|null $account
     *
     * @return $this
     */
    public function setAccount(?AccountUser $account): self
    {
        $this->account = $account;

        return $this;
    }
}