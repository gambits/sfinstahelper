<?php


namespace App\EntityTraits;

use App\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Trait UserTrait
 */
trait UserTrait
{
    /**
     * @var User|null
     * @Assert\NotNull(message="error.validation.not_null.accountDirectMessage.user")
     * @ORM\OneToOne(targetEntity="User")
     */
    private $user;

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     *
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}